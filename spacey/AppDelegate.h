//
//  AppDelegate.h
//  code_camp_2015
//
//  Created by Kraig Wastlund on 11/12/15.
//  Copyright © 2015 Kraig. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) AVAudioPlayer *backgroundMusicPlayer;

@end

