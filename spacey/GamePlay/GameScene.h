//
//  GameScene.h
//  code_camp_2015
//

//  Copyright (c) 2015 Kraig. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "Level.h"

typedef enum {
    kGameWon,
    kGameLost,
    kGameQuit,
} FinishedGameCode;

@protocol GameSceneDelegate <NSObject>

@optional

- (void)gameScene:(SKScene*)scene didFinishWithGameCode:(FinishedGameCode)gameCode;

@end

@protocol NumberOfMovesDelegate <NSObject>

@optional

- (void)gameScene:(SKScene*)scene didMakeMoveWithVector:(CGVector)vector;

@end

@interface GameScene : SKScene

@property (nonatomic, strong) NSNumber* levelNumber;
@property (nonatomic, assign) id<GameSceneDelegate> gameDelegate;
@property (nonatomic, assign) id<NumberOfMovesDelegate> movesDelegate;
@property (nonatomic, strong) Level* gLevel;

- (void)startSound;
- (void)stopSound;

@end
