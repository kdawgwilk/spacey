//
//  GameViewController.h
//  code_camp_2015
//

//  Copyright (c) 2015 Kraig. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>
#import "GameScene.h"
#import "SPCYAlertView.h"

@interface GameViewController : UIViewController <GameSceneDelegate, NumberOfMovesDelegate, SPCYAlertDelegate>

@property (nonatomic, strong) NSNumber* levelNumber;

@end
