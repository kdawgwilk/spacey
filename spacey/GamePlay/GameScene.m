
//
//  GameScene.m
//  nodes1
//
//  Created by Kraig Wastlund on 11/9/15.
//  Copyright (c) 2015 Kraig. All rights reserved.
//

#import "GameScene.h"
#import "Node.h"
#import "Player.h"
#import "WormHole.h"
@import AVFoundation;
#import "SPCYSettings.h"

static const uint32_t playerCategory        =  0x1 << 0;
static const uint32_t middleCategory        =  0x1 << 1;
static const uint32_t wormHoleCategory      =  0x1 << 2;
static const uint32_t blackHoleCategory     =  0x1 << 3;
static const uint32_t endCategory           =  0x1 << 4;


@interface GameScene() <SKPhysicsContactDelegate>

@property (nonatomic, strong) Player* player;
@property (nonatomic) CGVector playerVector;
@property (nonatomic) BOOL playerIsInsideNode;
@property (nonatomic) BOOL gameIsOver;

@property (nonatomic, strong) NSMutableArray* blackHolePoints;

@property (nonatomic) BOOL arcingAroundBlackHole;
@property (nonatomic, strong) NSMutableArray* arcPoints;
@property (nonatomic) CGPoint arcToPoint;

@property (nonatomic) CGPoint firstTouchPoint;

//@property (nonatomic, strong) SKSpriteNode* backButton;

@property (nonatomic) AVAudioPlayer * shipIdlePlayer;

@end

@implementation GameScene

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    // this gets called first
    // load sound, ai, unit stats that don't change
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        // do stuff here
        self.physicsWorld.gravity = CGVectorMake(0, 0);
        self.playerIsInsideNode = YES;
        self.physicsWorld.contactDelegate = self;
        self.blackHolePoints = [[NSMutableArray alloc] init];
        self.gameIsOver = NO;
        self.firstTouchPoint = CGPointMake(0, 0);
    }
    
    return self;
}

-(void)didMoveToView:(SKView*)view
{
    // cache visual elements
    // skscene gets loaded by the kenoarchiver
    // and all the png's and visual elements are going
    // to get loaded well before you get to here
//    [view setFrame:CGRectMake(6, 6, self.view.frame.size.width - 12, self.view.frame.size.width - 12)];
    [Player setPlayersPlistToLevel:self.levelNumber];
    
    [self startSound];
    
    self.player = [Player getPlayerWithCellSize:self.gLevel.start.size];
    [self.player setPosition:self.gLevel.start.position];
    [self.player setCurrentNode:self.gLevel.start];
    [self.player setZPosition:10];
    self.player.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:self.player.size.height * 0.25];
    self.player.physicsBody.dynamic = YES;
    self.player.physicsBody.categoryBitMask = playerCategory;
    self.player.physicsBody.contactTestBitMask = endCategory | middleCategory | blackHoleCategory | wormHoleCategory;
    self.player.physicsBody.affectedByGravity = NO;
    [self addChild:self.player];
    
    self.gLevel.start.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:self.gLevel.start.size.width * 0.2];
    self.gLevel.start.physicsBody.dynamic = YES;
    self.gLevel.start.physicsBody.categoryBitMask = middleCategory;
    self.gLevel.start.physicsBody.contactTestBitMask = playerCategory;
    self.gLevel.start.physicsBody.collisionBitMask = 0;
    [self addChild:self.gLevel.start];
    
    self.gLevel.end.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:self.gLevel.end.size.width * 0.2];
    self.gLevel.end.physicsBody.dynamic = YES;
    self.gLevel.end.physicsBody.categoryBitMask = endCategory;
    self.gLevel.end.physicsBody.contactTestBitMask = playerCategory;
    self.gLevel.end.physicsBody.collisionBitMask = 0;
    [self addChild:self.gLevel.end];
    [self drawPlanetBehindNode:self.gLevel.end];
    
    for (Node* node in self.gLevel.nodes)
    {
        node.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:node.size.width * 0.1];
        node.physicsBody.dynamic = YES;
        node.physicsBody.categoryBitMask = middleCategory;
        if (node.type.integerValue == kBlackHole)
        {
            node.physicsBody.categoryBitMask = blackHoleCategory;
            [self.blackHolePoints addObject:[NSValue valueWithCGPoint:node.position]];
        }
        node.physicsBody.contactTestBitMask = playerCategory;
        node.physicsBody.collisionBitMask = 0;
        [self addChild:node];
    }
    
    for (Node* node in self.gLevel.wormHoles)
    {
        node.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:node.size.width * 0.2];
        node.physicsBody.dynamic = YES;
        node.physicsBody.categoryBitMask = wormHoleCategory;
        node.physicsBody.contactTestBitMask = playerCategory;
        node.physicsBody.collisionBitMask = 0;
        [self drawPlanetBehindNode:node];
        [self addChild:node];
    }
    
    self.arcingAroundBlackHole = NO;
}

- (void)stopSound
{
    [self.shipIdlePlayer stop];
}

- (void)startSound
{
    NSError *error;
    NSURL * backgroundMusicURL = [[NSBundle mainBundle] URLForResource:@"ship_idle" withExtension:@"mp3"];
    self.shipIdlePlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:backgroundMusicURL error:&error];
    self.shipIdlePlayer.numberOfLoops = -1;
    [self.shipIdlePlayer prepareToPlay];
    [self.shipIdlePlayer setVolume:0.1];
    [self.shipIdlePlayer play];
}

- (void)drawPlanetBehindNode:(Node*)node
{
    CGSize circleSize = CGSizeMake(node.size.width * 0.9, node.size.height * 0.9);
    CGRect circle = CGRectMake(node.position.x - circleSize.width / 2, node.position.y - circleSize.height / 2, circleSize.width, circleSize.height);
    SKShapeNode *shapeNode = [[SKShapeNode alloc] init];
    shapeNode.path = [UIBezierPath bezierPathWithOvalInRect:circle].CGPath;
    shapeNode.fillColor = [[SPCYSettings endPlanetColors] objectAtIndex:arc4random() % [[SPCYSettings endPlanetColors] count]];
    shapeNode.lineWidth = 0;
    if (node.type.integerValue == kWormHole)
    {
        NSInteger colorIndex = [(WormHole*)node linkId].integerValue - 1;
        shapeNode.fillColor = [[SPCYSettings wormHoleColors] objectAtIndex:colorIndex % [[SPCYSettings wormHoleColors] count]];
    }
    [self addChild:shapeNode];
}

- (CGPoint)adjacentBlackHolePoint
{
    // first check the players distance from all blackholes
    // if i'm close to one then populate the bhPoint
    CGFloat minRadius = self.gLevel.cellSize.width * 1.05; // might need to factor pixel density here.
    CGPoint bhPoint = CGPointMake(0, 0);
    
    CGPoint playerPos = self.player.position;
    for (id pos in self.blackHolePoints)
    {
        CGPoint thisBHPoint = [pos CGPointValue];
        CGFloat xDist = (playerPos.x - thisBHPoint.x);
        CGFloat yDist = (playerPos.y - thisBHPoint.y);
        CGFloat distance = sqrt((xDist * xDist) + (yDist * yDist));
        if (distance <= minRadius && !self.playerIsInsideNode)
        {
            bhPoint = thisBHPoint;
            break;
        }
    }
    
    // now make sure the player is'nt headed right for that blackhole
    // and if i'm along side the bh, check to see if i've already passed it
    float maxMargin = 5.0;
    if (bhPoint.x != 0 || bhPoint.y != 0)
    {
        if (self.playerVector.dy > 0) // moving upwards
        {
            // first check if the player is headed right for the bh
            if (fabs(self.player.position.x - bhPoint.x) < maxMargin)
            {
                return CGPointMake(0, 0);
            }
            
            if (bhPoint.y < self.player.position.y) // i've already past the blackhole
            {
                bhPoint = CGPointMake(0, 0);
            }
        }
        else if (self.playerVector.dy < 0) // moving downwards
        {
            // first check if the player is headed right for the bh
            if (fabs(self.player.position.x - bhPoint.x) < maxMargin)
            {
                return CGPointMake(0, 0);
            }
            
            if (bhPoint.y > self.player.position.y) // i've already past the blackhole
            {
                bhPoint = CGPointMake(0, 0);
            }
        }
        else if (self.playerVector.dx < 0) // moving left
        {
            // first check if the player is headed right for the bh
            if (fabs(self.player.position.y - bhPoint.y) < maxMargin)
            {
                return CGPointMake(0, 0);
            }
            
            if (bhPoint.x > self.player.position.x) // i've already past the blackhole
            {
                bhPoint = CGPointMake(0, 0);
            }
        }
        else if (self.playerVector.dx > 0) // moving right
        {
            // first check if the player is headed right for the bh
            if (fabs(self.player.position.y - bhPoint.y) < maxMargin)
            {
                return CGPointMake(0, 0);
            }
            
            if (bhPoint.x < self.player.position.x) // i've already past the blackhole
            {
                bhPoint = CGPointMake(0, 0);
            }
        }
    }
    
    return bhPoint;
}

-(void)update:(CFTimeInterval)currentTime
{
    // if i'm not currently going around a black hole and I DO have speed (i'm moving)
    if (!self.arcingAroundBlackHole && (self.playerVector.dx != 0 || self.playerVector.dy != 0))
    {
        SKAction* move = [SKAction moveBy:self.playerVector duration:0];
        [self.player runAction:move];
    }
    else if (self.playerIsInsideNode) // i'm inside a node and not moving
    {
        return;
    }
    else if (self.arcingAroundBlackHole) // i'm currently going around a black hole
    {
        CGPoint thisPoint = [self.arcPoints[0] CGPointValue];
        [self.arcPoints removeObjectAtIndex:0];
        SKAction* move = [SKAction moveTo:thisPoint duration:0];
        [self.player runAction:move completion:^{
            if ([self.arcPoints count] == 0)
            {
                self.arcingAroundBlackHole = NO;
                self.arcPoints = nil;
                SKAction* move = [SKAction moveBy:self.playerVector duration:0];
                [self.player runAction:move];
            }
        }];
    }
    
    if ([self playerHasLeftScreen])
    {
        [self performSelector:@selector(gameOverWithCode:) withObject:@0 afterDelay:0.5];
        [self.scene setPaused:YES];
        return;
    }
    
    CGPoint possibleBlackHolePoint = [self adjacentBlackHolePoint];
    if (possibleBlackHolePoint.x != 0 && possibleBlackHolePoint.y != 0 && !self.arcingAroundBlackHole)
    {
        NSArray* points = [self controlPointsForStartingPoint:self.player.position blackHolePoint:possibleBlackHolePoint andDirection:self.playerVector];
        self.playerVector = [self vectorForBHPoint:possibleBlackHolePoint]; // set the players direction, to be used after he's done going around the bh
        self.arcPoints = [[Player pointsForCircleFromPoint:self.player.position toPoint:[points[2] CGPointValue] usingOrigin:possibleBlackHolePoint] mutableCopy];
        self.arcingAroundBlackHole = YES;
    }
}

- (void)gameOverWithCode:(NSNumber*)codeNum
{
    [self.scene setPaused:YES];
    
    // 0 = quit, 1 = lost, 2 = win
    if (codeNum.integerValue == 0)
    {
        [self.gameDelegate gameScene:self didFinishWithGameCode:kGameQuit];
    }
    else if (codeNum.integerValue == 1)
    {
        [self.gameDelegate gameScene:self didFinishWithGameCode:kGameLost];
    }
    else if (codeNum.integerValue == 2)
    {
        [self.gameDelegate gameScene:self didFinishWithGameCode:kGameWon];
    }
    
}

- (CGVector)vectorForBHPoint:(CGPoint)bhPoint
{
    CGPoint sPoint = self.player.position;
    CGVector vector = self.playerVector;
    
    CGVector retVector;
    if (vector.dx != 0) // moving horizontally
    {
        if (bhPoint.y > sPoint.y) // player is below the blackhole
        {
            retVector = CGVectorMake(0, self.player.speed);
        }
        else // player is above the blackhole
        {
            retVector = CGVectorMake(0, -self.player.speed);
        }
    }
    else // moving vertically
    {
        if (bhPoint.x > sPoint.x) // player is to the left of the blackhole
        {
            retVector = CGVectorMake(self.player.speed, 0);
        }
        else // player is to the right of the blackhole
        {
            retVector = CGVectorMake(-self.player.speed, 0);
        }
    }
    
    return retVector;
}

- (BOOL)playerHasLeftScreen
{
    if (self.player.position.x + self.player.size.width < 0)
    {
        return YES;
    }
    if (self.player.position.y + self.player.size.height < 0)
    {
        return YES;
    }
    if (self.player.position.x - self.player.size.width > self.view.frame.size.width)
    {
        return YES;
    }
    if (self.player.position.y - self.player.size.height > self.view.frame.size.height)
    {
        return YES;
    }

    return NO;
}

- (SKAction*)curveActionWithStartingPoint:(CGPoint)startingPoint blackHolePoint:(CGPoint)blackHolePoint
{
    CGMutablePathRef cgpath = CGPathCreateMutable();

    NSArray* curvePoints = [self controlPointsForStartingPoint:startingPoint blackHolePoint:blackHolePoint andDirection:self.playerVector];
    
    CGPoint controlPoint1 = [curvePoints[0] CGPointValue];
    CGPoint controlPoint2 = [curvePoints[1] CGPointValue];
    CGPoint endingPoint = [curvePoints[2] CGPointValue];

    CGPathMoveToPoint(cgpath, NULL, startingPoint.x, startingPoint.y);
    CGPathAddCurveToPoint(cgpath, NULL, controlPoint1.x, controlPoint1.y, controlPoint2.x, controlPoint2.y, endingPoint.x, endingPoint.y);

    SKAction* playerAroundBHCurve = [SKAction followPath:cgpath asOffset:NO orientToPath:YES duration:5];
    
    return [SKAction sequence:@[[SKAction waitForDuration:1],playerAroundBHCurve]];
}

- (NSArray*)controlPointsForStartingPoint:(CGPoint)sPoint blackHolePoint:(CGPoint)bhPoint andDirection:(CGVector)vector
{
    CGPoint endingPoint;
    CGPoint point1;
    CGPoint point2;
    
    CGFloat xDelta = self.gLevel.cellSize.width * 0.25;
    CGFloat yDelta = self.gLevel.cellSize.height * 0.25;
    
    if (vector.dx != 0) // moving horizontally
    {
        if (vector.dx < 0) // moving left
        {
            endingPoint.x = bhPoint.x - self.gLevel.cellSize.width;
            endingPoint.y = bhPoint.y;
            
            if (bhPoint.y > sPoint.y) // player is below the blackhole
            {
                point1 = CGPointMake(sPoint.x - xDelta, sPoint.y - yDelta);
                point2 = CGPointMake(bhPoint.x - xDelta, bhPoint.y - yDelta);
            }
            else // player is above the blackhole
            {
                point1 = CGPointMake(sPoint.x - xDelta, sPoint.y + yDelta);
                point2 = CGPointMake(bhPoint.x - xDelta, bhPoint.y + yDelta);
            }
        }
        else // moving right
        {
            endingPoint.x = bhPoint.x + self.gLevel.cellSize.width;
            endingPoint.y = bhPoint.y;
            
            if (bhPoint.y > sPoint.y) // player is below the blackhole
            {
                point1 = CGPointMake(sPoint.x + xDelta, sPoint.y - yDelta);
                point2 = CGPointMake(bhPoint.x + xDelta, bhPoint.y - yDelta);
            }
            else // player is above the blackhole
            {
                point1 = CGPointMake(sPoint.x + xDelta, sPoint.y + yDelta);
                point2 = CGPointMake(bhPoint.x + xDelta, bhPoint.y + yDelta);
            }
        }
    }
    else // moving vertically
    {
        if (vector.dy < 0) // moving down
        {
            endingPoint.x = bhPoint.x;
            endingPoint.y = bhPoint.y - self.gLevel.cellSize.height;
            
            if (bhPoint.x > sPoint.x) // player is to the left of the blackhole
            {
                point1 = CGPointMake(sPoint.x - xDelta, sPoint.y - yDelta);
                point2 = CGPointMake(bhPoint.x - xDelta, bhPoint.y - yDelta);
            }
            else // player is to the right of the blackhole
            {
                point1 = CGPointMake(sPoint.x + xDelta, sPoint.y - yDelta);
                point2 = CGPointMake(bhPoint.x + xDelta, bhPoint.y - yDelta);
            }
        }
        else // moving up
        {
            endingPoint.x = bhPoint.x;
            endingPoint.y = bhPoint.y + self.gLevel.cellSize.height;
            
            if (bhPoint.x > sPoint.x) // player is to the left of the blackhole
            {
                point1 = CGPointMake(sPoint.x - xDelta, sPoint.y + yDelta);
                point2 = CGPointMake(bhPoint.x - xDelta, bhPoint.y + yDelta);
            }
            else // player is to the right of the blackhole
            {
                point1 = CGPointMake(sPoint.x + xDelta, sPoint.y + yDelta);
                point2 = CGPointMake(bhPoint.x + xDelta, bhPoint.y + yDelta);
            }
        }
    }

    return @[[NSValue valueWithCGPoint:point1], [NSValue valueWithCGPoint:point2], [NSValue valueWithCGPoint:endingPoint]];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
        
    self.firstTouchPoint = [[touches anyObject] locationInView:self.view];
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    
    if (self.firstTouchPoint.x != 0 && self.firstTouchPoint.y != 0)
    {
        if (self.playerVector.dx != 0 || self.playerVector.dy != 0)
        {
            return;
        }
        
        UITouch* lastTouch = [touches anyObject];
        
        CGPoint location = [lastTouch locationInView:self.view];
        CGPoint prevLocation = self.firstTouchPoint;
        
        float dx = prevLocation.x - location.x;
        float dy = prevLocation.y - location.y;
        
        if (fabs(dx) > fabs(dy)) // swipe is mostly horizontal
        {
            if (fabs(dx) > 50) // the horizontal swipe is more than 10 points
            {
                if (dx > 0) // the horizontal swipe is right
                {
                    self.playerVector = CGVectorMake(-self.player.speed, 0.0);
                }
                else // the horizontal swipe is left
                {
                    self.playerVector = CGVectorMake(self.player.speed, 0.0);
                }
            }
        }
        else // swipe is mostly vertical
        {
            if (fabs(dy) > 50) // the vertical swipe is more than 10 points
            {
                if (dy > 0) // the vertical swipe is up
                {
                    self.playerVector = CGVectorMake(0.0, self.player.speed);
                }
                else // the vertical swipe is down
                {
                    self.playerVector = CGVectorMake(0.0, -self.player.speed);
                }
            }
        }
    }
    
    self.firstTouchPoint = CGPointMake(0, 0);
    if (self.playerVector.dx != 0 || self.playerVector.dy != 0)
    {
        [self.movesDelegate gameScene:self didMakeMoveWithVector:self.playerVector];
        [self runAction:[SKAction playSoundFileNamed:@"rev.mp3" waitForCompletion:NO]];
    }
}

- (void)didBeginContact:(SKPhysicsContact *)contact
{
    if (self.playerIsInsideNode == YES || self.arcingAroundBlackHole)
    {
        return;
    }
    self.playerIsInsideNode = YES;
    
    SKPhysicsBody* playerBody;
    SKPhysicsBody* secondBody;
    
    if (contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask && [contact.bodyA.node.name isEqualToString:@"player"])
    {
        playerBody = contact.bodyA;
        secondBody = contact.bodyB;
    }
    else if ([contact.bodyB.node.name isEqualToString:@"player"])
    {
        playerBody = contact.bodyB;
        secondBody = contact.bodyA;
    }
    
    if (playerBody && secondBody)
    {
        SKAction* move;
        BOOL didWin = NO;
        BOOL didDie = NO;
        BOOL didTravelThruWormhole = NO;
        
        if ([secondBody.node isKindOfClass:[WormHole class]])
        {
            WormHole* wormhole = (WormHole*)secondBody.node;
            move = [SKAction moveTo:wormhole.partnerNode.position duration:0.0];
            didTravelThruWormhole = YES;
        }
        else if (secondBody.categoryBitMask == endCategory)
        {
            move = [SKAction moveTo:secondBody.node.position duration:0.0];
            self.playerVector = CGVectorMake(0, 0);
            self.gameIsOver = YES;
            didWin = YES;
        }
        else if (secondBody.categoryBitMask == blackHoleCategory)
        {
            move = [SKAction moveTo:secondBody.node.position duration:0.0];
            self.playerVector = CGVectorMake(0, 0);
            
            didDie = YES;
        }
        else // middle
        {
            move = [SKAction moveTo:secondBody.node.position duration:0.0];
            self.playerVector = CGVectorMake(0, 0);
        }
        
        [self.player runAction:move completion:^{
            if (didTravelThruWormhole)
            {
                self.playerIsInsideNode = NO;
                SKAction* move = [SKAction moveBy:self.playerVector duration:0.0];
                [self.player runAction:move completion:^{
                    [self.player runAction:move];
                }];
            }
        }];
        
        if (didWin) // landed on end
        {
            [self performSelector:@selector(didWin) withObject:nil afterDelay:0.25];
        }
        else if (didDie) // landed on black hole
        {
            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
            [self runAction:[SKAction playSoundFileNamed:@"die.mp3" waitForCompletion:NO]];
            SKAction* rotateAction = [SKAction rotateByAngle:M_PI*4 duration:10];
            SKAction* scaleAction = [SKAction scaleTo:0.0 duration:10];
            [self.player runAction:rotateAction];
            [self.player runAction:scaleAction completion:^{
                [self performSelector:@selector(gameOverWithCode:) withObject:@1 afterDelay:0.5];
            }];
        }
    }
}

- (void)didEndContact:(SKPhysicsContact *)contact
{
    self.playerIsInsideNode = NO;
}

- (void)didWin
{
    SKAction* scaleAction = [SKAction scaleTo:0.0 duration:10];
    [self.player runAction:scaleAction completion:^{
        NSNumber* nextLevel = [NSNumber numberWithInteger:self.levelNumber.integerValue + 1];
        if (self.player.currentLevel.integerValue < nextLevel.integerValue)
        {
            if (nextLevel.integerValue < [Level getTotalNumberOfLevels])
            {
                self.player.currentLevel = nextLevel;
            }
        }
        [self.player savePlayer];
        [self performSelector:@selector(gameOverWithCode:) withObject:@2 afterDelay:0.5];
    }];
}

@end
