//
//  GameViewController.m
//  code_camp_2015
//
//  Created by Kraig Wastlund on 11/12/15.
//  Copyright (c) 2015 Kraig. All rights reserved.
//

#import "GameViewController.h"
#import "GameScene.h"
#import "Level.h"
#import "Player.h"
#import "SoundController.h"
#import "SPCYSettings.h"

@interface GameViewController()

@property (nonatomic, strong) SKView* skView;
@property (nonatomic, strong) UILabel* timerLabel;
@property (nonatomic, strong) NSTimer* timer;
@property (nonatomic, strong) UILabel* numOfMovesLabel;
@property (nonatomic, strong) NSDate* timerStartDate;
@property (nonatomic) NSInteger numOfMoves;
@property (nonatomic, strong) UILabel* titleLabel;
@property (nonatomic, strong) UIButton* backButton;

@property (nonatomic) CGRect menuButtonRect;
@property (nonatomic) CGRect retryButtonRect;
@property (nonatomic) CGRect playNextButtonRect;
@property (nonatomic) BOOL menuButtonActive;
@property (nonatomic) BOOL retryButtonActive;
@property (nonatomic) BOOL playNextButtonActive;

@property (nonatomic, weak) GameScene* gameScene;
@property (nonatomic, strong) SKSpriteNode* ship;

@end

@implementation SKScene (Unarchive)

+ (instancetype)unarchiveFromFile:(NSString *)file {
    /* Retrieve scene file path from the application bundle */
    NSString *nodePath = [[NSBundle mainBundle] pathForResource:file ofType:@"sks"];
    /* Unarchive the file to an SKScene object */
    NSData *data = [NSData dataWithContentsOfFile:nodePath
                                          options:NSDataReadingMappedIfSafe
                                            error:nil];
    NSKeyedUnarchiver *arch = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
    [arch setClass:self forClassName:@"SKScene"];
    SKScene *scene = [arch decodeObjectForKey:NSKeyedArchiveRootObjectKey];
    [arch finishDecoding];
    
    return scene;
}

@end

@implementation GameViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Configure the view.
    self.skView = [[SKView alloc] init];
    [self.skView setFrame:self.view.frame];
    self.skView.ignoresSiblingOrder = YES;
//    self.skView.showsFPS = YES;
//    self.skView.showsDrawCount = YES;
//    self.skView.showsNodeCount = YES;
//    self.skView.showsQuadCount = YES;
    [self.skView setBackgroundColor:[UIColor clearColor]];
    
    UIImageView* backgroundImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"background"]];
    [backgroundImage setFrame:self.view.frame];
    [self.view addSubview:backgroundImage];
    
    [self.view addSubview:self.skView];
    
    [self startGame];
}

- (void)setupTimerLabel
{
    if (!self.timerLabel)
    {
        self.timerLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 66, self.view.frame.size.height - 40, 60, 24)];
        [self.timerLabel setFont:[UIFont systemFontOfSize:20 weight:UIFontWeightUltraLight]];
        [self.timerLabel setTextColor:[UIColor whiteColor]];
        [self.timerLabel setTextAlignment:NSTextAlignmentLeft];
//        [self.timerLabel setBackgroundColor:[[UIColor redColor] colorWithAlphaComponent:1.0f]];
        [self.view addSubview:self.timerLabel];
    }
    
    double timePassed_ms = [self.timerStartDate timeIntervalSinceNow] * -1000.0;
    float seconds = timePassed_ms / 1000.0;
    float minutes = seconds / 60.0;
    
    self.timerLabel.text = [NSString stringWithFormat:@"%02ld:%02ld", (long)minutes, (long)seconds];
    
    if (!self.timer)
    {
        self.timer = [NSTimer scheduledTimerWithTimeInterval: 1.0 target:self selector:@selector(updateCountdown) userInfo:nil repeats:YES];
    }
}

- (void)setupBackButton
{
    if (!self.backButton)
    {
        NSInteger buttonWidth = 30;
        CGRect buttonRect = CGRectMake(self.view.frame.size.width / 2 - (buttonWidth / 2), 40, buttonWidth, buttonWidth);
        self.backButton = [[UIButton alloc] initWithFrame:buttonRect];
        [self.backButton setImage:[UIImage imageNamed:@"simple_button"] forState:UIControlStateNormal];
        [self.backButton addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:self.backButton];
        [self.view bringSubviewToFront:self.backButton];
    }
}

- (void)backButtonPressed
{
    SPCYAlertView* alertView = [[SPCYAlertView alloc] initWithLevelNumber:self.levelNumber worldsBest:@-81.98591697216034 currentTime:@([self.timerStartDate timeIntervalSinceNow]) bestPersonalTime:@-81.98591697216034 levelScore:0 andGameCode:kGameQuit];
    alertView.alertDelegate = self;
    [self.view addSubview:alertView];
}

- (void)setupNumOfMovesLabel
{
    if (!self.numOfMovesLabel)
    {
        self.numOfMovesLabel = [[UILabel alloc] initWithFrame:CGRectMake(14, self.view.frame.size.height - 40, 54, 24)];
        [self.numOfMovesLabel setFont:[UIFont systemFontOfSize:20 weight:UIFontWeightUltraLight]];
        [self.numOfMovesLabel setTextColor:[UIColor whiteColor]];
        [self.numOfMovesLabel setTextAlignment:NSTextAlignmentLeft];
        [self.view addSubview:self.numOfMovesLabel];
    }
    
    self.numOfMovesLabel.text = [NSString stringWithFormat:@"%ld", (long)self.numOfMoves];
}

- (void)setupTitleLabel
{
    if (!self.titleLabel)
    {
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 80, self.view.frame.size.width, 40)];
        
        [self.titleLabel setFont:[UIFont systemFontOfSize:24 weight:UIFontWeightUltraLight]];
        [self.titleLabel setTextColor:[UIColor whiteColor]];
        [self.titleLabel setTextAlignment:NSTextAlignmentCenter];
        [self.view addSubview:self.titleLabel];
        [self.view bringSubviewToFront:self.titleLabel];
    }
    
    [self.titleLabel setText:[NSString stringWithFormat:@"Planet %03ld", (long)self.levelNumber.integerValue]];
}

-(void) updateCountdown
{
    double timePassed_ms = [self.timerStartDate timeIntervalSinceNow] * -1000.0;
    float seconds = timePassed_ms / 1000.0;
    float minutes = seconds / 60.0;

    self.timerLabel.text = [NSString stringWithFormat:@"%02ld:%02ld", (long)minutes, (long)seconds];
}

- (void)startGame
{
    self.timerStartDate = [NSDate date];
    
    // Create and configure the scene.
    self.gameScene = [GameScene unarchiveFromFile:@"GameScene"];
    self.gameScene.scaleMode = SKSceneScaleModeResizeFill;
    [self.gameScene setSize:self.skView.frame.size];
    self.gameScene.gameDelegate = self;
    self.gameScene.movesDelegate = self;
    self.gameScene.levelNumber = self.levelNumber;
    self.gameScene.gLevel = [Level getLevelWithName:self.levelNumber width:[NSNumber numberWithDouble:self.view.frame.size.width - 12] height:[NSNumber numberWithDouble:self.view.frame.size.width - 12]];
    [self.gameScene setBackgroundColor:[UIColor clearColor]];
    
    self.numOfMoves = 0;
    
    [self setupTitleLabel];
    [self setupBackButton];
    [self setupTimerLabel];
    [self setupNumOfMovesLabel];
    
    [self.gameScene startSound];
    
    // Present the scene.
    [self.skView presentScene:self.gameScene];
}

#pragma mark - GameFinishedDelegate

- (void)gameScene:(SKScene*)scene didFinishWithGameCode:(FinishedGameCode)gameCode
{
//    [self.gameScene removeAllActions];
//    [self.gameScene removeAllChildren];
//    [self.gameScene removeFromParent];
//    self.gameScene = nil; // if u do this it'll lose the level
    [self.skView presentScene:nil];
//    [self.skView removeFromSuperview];
    
    [self.timer invalidate];
    self.timer = nil;
    [self.gameScene stopSound];
    NSDate* stopTimerDate = [NSDate date];
    
    Level* thisLevel = [Level getLevelWithName:self.levelNumber width:0 height:0];
    NSNumber* score = [Level getScoreForLevel:thisLevel withTime:@([self.timerStartDate timeIntervalSinceDate:stopTimerDate]) moves:[NSNumber numberWithInteger:self.numOfMoves]];
    
    float randomBest1 = ((float)rand() / RAND_MAX) * 5; // get random number up to 5
    float randomBest2 = ((float)rand() / RAND_MAX) * 5; // get random number up to 5
    SPCYAlertView* alertView = [[SPCYAlertView alloc] initWithLevelNumber:self.levelNumber worldsBest:[NSNumber numberWithFloat:-randomBest1] currentTime:@([self.timerStartDate timeIntervalSinceDate:stopTimerDate]) bestPersonalTime:[NSNumber numberWithFloat:-randomBest2] levelScore:score andGameCode:gameCode];
    alertView.alertDelegate = self;
    [self.view addSubview:alertView];
}

#pragma mark - NumberOfMovesDelegate

- (void)gameScene:(SKScene*)scene didMakeMoveWithVector:(CGVector)vector
{
    self.numOfMoves ++;
    
    [self.numOfMovesLabel setText:[NSString stringWithFormat:@"%ld", (long)self.numOfMoves]];
}

#pragma mark - AlertViewDelegate

- (void)alertView:(SPCYAlertView*)view didSelectWithIndex:(AlertButtonType)buttonType
{
    if (buttonType == kTypeLeft) // replay
    {
        [self startGame];
        [self removeAlertView:view];
    }
    else if (buttonType == kTypeMiddle) // play next
    {
        self.levelNumber = [NSNumber numberWithInteger:self.levelNumber.integerValue + 1];
        [self startGame];
        [self removeAlertView:view];
    }
    else if (buttonType == kTypeRight) // menu
    {
        [self.skView removeFromSuperview];
        [self performSegueWithIdentifier:@"Game To Menu" sender:nil];
    }
}

- (void)removeAlertView:(SPCYAlertView*)view
{
    [UIView animateWithDuration:0.2
                     animations:^{view.alpha = 0.0;}
                     completion:^(BOOL finished){ [view removeFromSuperview]; }];
}

@end
