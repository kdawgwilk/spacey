//
//  menuPlanet.m
//  spacey
//
//  Created by Chad Olsen on 11/22/15.
//  Copyright © 2015 Kraig. All rights reserved.
//

#import "MenuPlanet.h"
#import "MoonNode.h"

#define DEGREES_TO_RADIANS(angle) ((angle) / 180.0 * M_PI)

@interface MenuPlanet ()

@property (nonatomic, strong) SKSpriteNode* planet;
@property (nonatomic, strong) SKShapeNode* planetBackground;
@property (nonatomic, strong) NSMutableArray* moonsArray;
@property (nonatomic, strong) SKFieldNode* gravityField;

@end

@implementation MenuPlanet

- (instancetype)initWithLevel:(NSNumber*)levelNumber Position:(CGPoint)position dark:(BOOL)dark
{
    self = [super init];
    if (self)
    {
        self.isDark = dark;
        self.position = position;
        [self setupWorldNode];
    }
    return self;
}

- (void)setupWorldNode
{
    self.moonsArray = [[NSMutableArray alloc] init];
    [self addPlanetBehind];

    [self addWorld];

    [self animateWorldNode];
    
    for (int i = 0; i < 3; i++)
    {
        NSInteger orbirtDistance = (arc4random() % 20) + 40;
        CGFloat angleRadians = (arc4random() % 60) + (i * 60);
        
        CGFloat x = cos(DEGREES_TO_RADIANS(angleRadians)) * orbirtDistance;
        CGFloat y = sin(DEGREES_TO_RADIANS(angleRadians)) * orbirtDistance;
        [self addMoonAtPosition:CGPointMake(x, y) Darken:self.isDark];
    }

    [self addGravity];
}

- (void)animateWorldNode
{
    NSMutableArray* arrayOfTextures = [[NSMutableArray alloc] init];
    for (int i = 0; i <= 119; i++)
    {
        NSString* name = @"end";
        NSString* fileName = [MenuPlanet fileNameForName:name andIndex:i];
        [arrayOfTextures addObject:[SKTexture textureWithImageNamed:fileName]];
    }
    
    [self.planet runAction:[SKAction repeatActionForever:[SKAction animateWithTextures:arrayOfTextures timePerFrame:0.125]]];

}

- (void)removeMenuPlanet
{
    [self.planet removeAllActions];
    [self removeAllActions];
}

+ (NSString*)fileNameForName:(NSString*)name andIndex:(int)i
{
    NSString* imageName = [[NSString alloc] init];
    
    if (i < 10)
    {
        imageName = [NSString stringWithFormat:@"%@_00%d", name, i];
    }
    else if (i < 100)
    {
        imageName = [NSString stringWithFormat:@"%@_0%d", name, i];
    }
    else
    {
        imageName = [NSString stringWithFormat:@"%@_%d", name, i];
    }
    
    return imageName;
}

- (void)addPlanetBehind
{
    self.planetBackground = [SKShapeNode shapeNodeWithCircleOfRadius:23.0];
    self.planetBackground.fillColor = self.worldColor;
    self.planetBackground.strokeColor = [UIColor clearColor];
    self.planetBackground.zPosition = self.zPosition + 1.0;
    [self.planetBackground setPosition:CGPointMake(0, 0)];
    if (self.isDark)
    {
        self.planetBackground.fillColor = [UIColor darkGrayColor];
    }
    [self addChild:self.planetBackground];
}

-(void)addWorld
{
    self.planet = [[SKSpriteNode alloc] init];
    
    self.planet.position = CGPointMake(0, 0);
    self.planet.size = CGSizeMake(50, 50);
    self.planet.zPosition = self.zPosition + 1.0;
    
    if (self.isDark)
    {
        self.planet.color = [UIColor darkGrayColor];
        self.planet.colorBlendFactor = 0.75;
    }
    else
    {
        self.planet.color = [UIColor lightGrayColor];
        self.planet.colorBlendFactor = 0.75;
    }
    [self addChild:self.planet];
}

- (void) addMoonAtPosition:(CGPoint)position Darken:(BOOL)darken
{
    CGFloat radius = 5.0;
    MoonNode* moon = [MoonNode shapeNodeWithCircleOfRadius:radius];
    moon.fillColor = [UIColor yellowColor];
    moon.strokeColor = [UIColor clearColor];
    moon.zPosition = 50;
    moon.position = position;
    moon.parentPlanet = self;
    
    moon.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:radius];
    moon.physicsBody.affectedByGravity = NO;
    moon.physicsBody.allowsRotation = YES;    
    moon.physicsBody.mass = ((arc4random() % 40) + 10) / 100.0;
    moon.physicsBody.linearDamping = 0.0;
    moon.physicsBody.friction = 0;
    moon.physicsBody.collisionBitMask = 0;
    
    if (darken)
    {
        moon.fillColor = [UIColor darkGrayColor];
    }
    
    [self.moonsArray addObject:moon];

    [self addChild:moon];
}

- (void)addGravity
{
    self.gravityField = [SKFieldNode springField];
    self.gravityField.enabled = YES;
    self.gravityField.position = CGPointMake(0, 0);
    self.gravityField.strength = 0.5;
    self.gravityField.region = [[SKRegion alloc] initWithSize:CGSizeMake(80, 80)];
    [self addChild:self.gravityField];
}

- (void)movePlanetNode
{
    for (MoonNode* moon in self.moonsArray)
    {
        [moon moveMoon];
    }
}

@end
