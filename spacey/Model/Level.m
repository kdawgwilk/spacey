//
//  Level.m
//  code_camp_2015
//
//  Created by Donald Timpson on 11/13/15.
//  Copyright © 2015 Kraig. All rights reserved.
//

#import "Level.h"
#import "WormHole.h"

@implementation Level

- (Node*)start
{
    if (!_start)
    {
        _start = [[Node alloc] init];
    }
    
    return _start;
}

- (Node*)end
{
    if (!_end)
    {
        _end = [[Node alloc] init];
    }
    
    return _end;
}

- (NSArray*)nodes
{
    if (!_nodes)
    {
        _nodes = [[NSArray alloc] init];
    }
    
    return _nodes;
}

+ (Level*)getLevelWithName:(NSNumber*)levelNumber width:(NSNumber*)width height:(NSNumber*)height
{
    // grab the original plist from the GeneratedLevels dir
    NSString* levelName = [NSString stringWithFormat:@"Level%@", levelNumber];
    NSString* levelPath = [[NSBundle mainBundle] pathForResource:levelName ofType:@"plist"];
    
    NSDictionary* levelDictionary = [NSDictionary dictionaryWithContentsOfFile:levelPath];
    
    return [Level setupLevelWithDictionary:levelDictionary width:width height:height];
}

+ (Level*)setupLevelWithDictionary:(NSDictionary*)levelDict width:(NSNumber*)width height:(NSNumber*)height
{
    Level* newLevel = [[Level alloc] init];
    newLevel.width = width;
    newLevel.height = height;
    newLevel.numberOfRows = [levelDict objectForKey:@"rows"];
    newLevel.numberOfColumns = [levelDict objectForKey:@"columns"];
    
    CGFloat gridSquareWidth = newLevel.width.doubleValue / newLevel.numberOfColumns.doubleValue;
    CGFloat gridSquareHeight = newLevel.height.doubleValue / newLevel.numberOfRows.doubleValue;
    newLevel.cellSize = CGSizeMake(gridSquareWidth, gridSquareHeight);
    
    NSDictionary* startNodeDict = [levelDict objectForKey:@"start"];
    NSDictionary* endNodeDict = [levelDict objectForKey:@"end"];
    NSArray* middleNodeDicts = [levelDict objectForKey:@"middle"];
    NSArray* blackHoleNodeDicts = [levelDict objectForKey:@"blackHoles"];
    NSArray* wormHoleNodeDicts = [levelDict objectForKey:@"wormHoles"];
    
    NSMutableArray* middleNodes = [[NSMutableArray alloc] init];
    NSMutableArray* wormHoleNodes = [[NSMutableArray alloc] init];
    
    for (NSDictionary* middleNodeDict in middleNodeDicts)
    {
        [middleNodes addObject:[Node setupNodeWithDictionary:middleNodeDict forLevel:newLevel]];
    }
    
    for (NSDictionary* blackHoleDict in blackHoleNodeDicts)
    {
        [middleNodes addObject:[Node setupNodeWithDictionary:blackHoleDict forLevel:newLevel]];
    }
    
    for (NSDictionary* wormHoleNodeDict in wormHoleNodeDicts)
    {
        [wormHoleNodes addObject:[WormHole setupWormHoleWithDictionary:wormHoleNodeDict forLevel:newLevel]];
    }
    
    for (int i = 0; i < [wormHoleNodes count]; i++)
    {
        WormHole* wormHoleOne = wormHoleNodes[i];
        for (int j = 0; j < [wormHoleNodes count]; j++)
        {
            if (i != j && [wormHoleNodes[i] linkId].integerValue == [wormHoleNodes[j] linkId].integerValue)
            {
                WormHole* wormHoleTwo = wormHoleNodes[j];
                
                wormHoleOne.partnerNode = wormHoleTwo;
                wormHoleTwo.partnerNode = wormHoleOne;
                break;
            }
        }
    }
    
    newLevel.start = [Node setupNodeWithDictionary:startNodeDict forLevel:newLevel];
    newLevel.end = [Node setupNodeWithDictionary:endNodeDict forLevel:newLevel];
    newLevel.nodes = [NSArray arrayWithArray:middleNodes];
    newLevel.moves = [levelDict objectForKey:@"moves"];
    newLevel.wormHoles = [NSArray arrayWithArray:wormHoleNodes];
    
    if (isnan(newLevel.end.size.width))
    {
        NSLog(@"\n\n\nNAN\n\n\n");
    }
    
    return newLevel;
}

+ (NSNumber*)getScoreForLevel:(Level*)level withTime:(NSNumber*)time moves:(NSNumber*)moves
{
    double timeScore = 1;
    double moveScore = 1;
    
    if (time.doubleValue > (level.moves.doubleValue*5))
    {
        timeScore = ((level.moves.doubleValue*10) - (time.doubleValue)) / (level.moves.doubleValue*5);
        if (timeScore < 0)
        {
            timeScore = 0;
        }
    }
    
    if (moves > level.moves)
    {
        moveScore = (2*level.moves.doubleValue - moves.doubleValue) / level.moves.doubleValue;
        if (moveScore < 0)
        {
            moveScore = 0;
        }
    }
    
    return [NSNumber numberWithDouble:((timeScore + moveScore) / 2)];
}

+ (NSInteger)getTotalNumberOfLevels
{
    NSString *bundleRoot = [[NSBundle mainBundle] bundlePath];
    NSFileManager *fm = [NSFileManager defaultManager];
    NSArray *dirContents = [fm contentsOfDirectoryAtPath:bundleRoot error:nil];
    NSPredicate *fltr = [NSPredicate predicateWithFormat:@"self ENDSWITH '.plist' && self BEGINSWITH 'Level'"];
    NSArray *onlyPlists = [dirContents filteredArrayUsingPredicate:fltr];
        
    return [onlyPlists count];
}

@end
