//
//  Player.m
//  code_camp_2015
//
//  Created by Donald Timpson on 11/13/15.
//  Copyright © 2015 Kraig. All rights reserved.
//

#import "Player.h"

@interface Player()

@property (strong, nonatomic) NSString* filePath;

@end

@implementation Player

+ (Player*)getPlayerWithCellSize:(CGSize)cellSize
{
    NSString* playerPath = [(NSString*)[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingString:@"/Player.plist"];
    
//    if (![[NSFileManager defaultManager] fileExistsAtPath:playerPath])
//    {
//        [[NSFileManager defaultManager] copyItemAtPath:[[NSBundle mainBundle] pathForResource:@"Player" ofType:@"plist"] toPath:playerPath error:nil];
//    }
    
    NSDictionary* playerDictionary = [NSDictionary dictionaryWithContentsOfFile:playerPath];
    Player* newPlayer = [[Player alloc] init];
    
    newPlayer.speed = [(NSNumber*)[playerDictionary objectForKey:@"speed"] doubleValue];
    newPlayer.currentLevel = [playerDictionary objectForKey:@"currentLevel"];
    newPlayer.filePath = playerPath;
    newPlayer.name = @"player";
    newPlayer.size = [Player scalePlayerWithCellSize:cellSize];
    
    NSMutableArray* arrayOfTextures = [[NSMutableArray alloc] init];
    for (int i = 0; i <= 119; i++)
    {
        NSString* name = @"ship wiggling";
        NSString* fileName = [Player fileNameForName:name andIndex:i];
        [arrayOfTextures addObject:[SKTexture textureWithImageNamed:fileName]];
    }
    
    [newPlayer runAction:[SKAction repeatActionForever:[SKAction animateWithTextures:arrayOfTextures timePerFrame:0.125]]];
    
    return newPlayer;
}

+ (CGSize)scalePlayerWithCellSize:(CGSize)cellSize
{
    CGSize playerSize;
    
    CGFloat cellWidth = cellSize.width;
    CGFloat cellHeight = cellSize.height;
    CGFloat playerWidth = cellWidth * 2.2;
    CGFloat playerHeight = cellHeight * 1.6;
    playerSize.width = playerWidth;
    playerSize.height = playerHeight;
    
    return playerSize;
}

- (void)savePlayer
{
    NSDictionary* playerDict = @{ @"speed" : [NSNumber numberWithDouble:self.speed],
                                  @"currentLevel" : self.currentLevel };
    
    [playerDict writeToFile:self.filePath atomically:YES];
}

+ (NSArray*)pointsForCircleFromPoint:(CGPoint)fromPoint toPoint:(CGPoint)toPoint usingOrigin:(CGPoint)origin
{
    NSMutableArray* allPoints = [[NSMutableArray alloc] init];
    
    double numberOfPoints = fabs(fromPoint.x - toPoint.x) / 6 - 2;
    double xInc = (toPoint.x - fromPoint.x) / numberOfPoints;
    double radiusSquare = (toPoint.x - origin.x)*(toPoint.x - origin.x) + (toPoint.y - origin.y)*(toPoint.y - origin.y);
    
    CGFloat x = fromPoint.x;
    CGFloat y = fromPoint.y;
    
    if (((toPoint.y + fromPoint.y)/2) > origin.y)
    {
        for (int i = 0; i < numberOfPoints; i++)
        {
            x += xInc;
            y = sqrt(radiusSquare - (x - origin.x)*(x - origin.x)) + origin.y;
            
            [allPoints addObject:[NSValue valueWithCGPoint:CGPointMake(x, y)]];
        }
        
        [allPoints addObject:[NSValue valueWithCGPoint:toPoint]];
    }
    else
    {
        for (int i = 0; i < numberOfPoints; i++)
        {
            x += xInc;
            y = -sqrt(radiusSquare - (x - origin.x)*(x - origin.x)) + origin.y;
            
            [allPoints addObject:[NSValue valueWithCGPoint:CGPointMake(x, y)]];
        }
        
        [allPoints addObject:[NSValue valueWithCGPoint:toPoint]];
    }
    
    return allPoints;
}

+ (NSString*)fileNameForName:(NSString*)name andIndex:(int)i
{
    NSString* imageName = [[NSString alloc] init];
    
    if (i < 10)
    {
        imageName = [NSString stringWithFormat:@"%@_0000%d", name, i];
    }
    else if (i < 100)
    {
        imageName = [NSString stringWithFormat:@"%@_000%d", name, i];
    }
    else
    {
        imageName = [NSString stringWithFormat:@"%@_00%d", name, i];
    }
    
    return imageName;
}

+ (NSNumber*)getPlayersCurrentLevelFromPlist // talking about the plist in the documents directory that is mutable
{
    NSString* plistPath = [(NSString*)[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingString:@"/Player.plist"];
    NSDictionary* playerDict = [NSDictionary dictionaryWithContentsOfFile:plistPath];
    
    return [playerDict objectForKey:@"currentLevel"];
}

+ (void)setPlayersPlistToLevel:(NSNumber*)levelNumber // talking about the plist in the documents directory that is mutable
{
    NSString* plistPath = [(NSString*)[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingString:@"/Player.plist"];
    NSMutableDictionary* playerDict = [[NSDictionary dictionaryWithContentsOfFile:plistPath] mutableCopy];
    
    NSNumber* levelOnPlist = [playerDict objectForKey:@"currentLevel"];
    
    if  (levelNumber.integerValue > levelOnPlist.integerValue)
    {
        [playerDict setObject:levelNumber forKey:@"currentLevel"];
        [playerDict writeToFile:plistPath atomically:YES];
    }
}

@end
