//
//  SPCYSettings.h
//  spacey
//
//  Created by Kraig Wastlund on 11/18/15.
//  Copyright © 2015 Kraig. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface SPCYSettings : NSObject

+ (NSArray*)wormHoleColors;
+ (NSArray*)endPlanetColors;
+ (NSString*)gameCompletedMessage;
+ (NSString*)threeMoonsMessage;
+ (NSString*)twoMoonsMessage;
+ (NSString*)oneMoonsMessage;
+ (NSString*)zeroMoonsMessage;
+ (UIColor*)colorFromHexString:(NSString*)hexString;
+ (UIColor*)playButtonColor;
+ (UIColor*)menuButtonColor;
+ (UIColor*)settingsButtonColor;
+ (UIColor*)restartButtonColor;
+ (UIColor*)levelsButtonColor;
+ (UIColor*)quitButtonsColor;


@end
