//
//  menuPlanet.h
//  spacey
//
//  Created by Chad Olsen on 11/22/15.
//  Copyright © 2015 Kraig. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface MenuPlanet : SKNode

@property (nonatomic) BOOL isDark;
@property (nonatomic, strong) UIColor* worldColor;

- (void)setupWorldNode;

- (void)animateWorldNode;

- (void)removeMenuPlanet;

+ (NSString*)fileNameForName:(NSString*)name andIndex:(int)i;

- (void)movePlanetNode;

- (instancetype)initWithLevel:(NSNumber*)levelNumber Position:(CGPoint)position dark:(BOOL)dark;

@end
