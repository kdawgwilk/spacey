//
//  Player.h
//  code_camp_2015
//
//  Created by Donald Timpson on 11/13/15.
//  Copyright © 2015 Kraig. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "Node.h"

@interface Player : Node

@property (strong, nonatomic) NSNumber* dx;
@property (strong, nonatomic) NSNumber* dy;
@property (strong, nonatomic) NSNumber* currentLevel;
@property (strong, nonatomic) Node* currentNode;

+ (Player*)getPlayerWithCellSize:(CGSize)size;

- (void)savePlayer;

+ (NSArray*)pointsForCircleFromPoint:(CGPoint)fromPoint toPoint:(CGPoint)toPoint usingOrigin:(CGPoint)origin;

+ (NSNumber*)getPlayersCurrentLevelFromPlist;

+ (void)setPlayersPlistToLevel:(NSNumber*)levelNumber;

@end
