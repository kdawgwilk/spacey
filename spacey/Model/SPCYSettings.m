//
//  SPCYSettings.m
//  spacey
//
//  Created by Kraig Wastlund on 11/18/15.
//  Copyright © 2015 Kraig. All rights reserved.
//

#import "SPCYSettings.h"
#import <UIKit/UIKit.h>

@implementation SPCYSettings

+ (NSArray*)wormHoleColors
{
    return @[
             [[UIColor greenColor] colorWithAlphaComponent:0.10],
             [[UIColor whiteColor] colorWithAlphaComponent:0.15],
             [[UIColor redColor] colorWithAlphaComponent:0.10],
             [[UIColor blueColor] colorWithAlphaComponent:0.10],
             [[UIColor yellowColor] colorWithAlphaComponent:0.17],
             ];
}

+ (NSArray*)endPlanetColors
{
    return @[
            [UIColor colorWithRed:1 green:0 blue:0 alpha:1],
            [UIColor colorWithRed:0.25 green:0 blue:1 alpha:1],
            [UIColor colorWithRed:1 green:0.25 blue:0 alpha:1],
            [UIColor colorWithRed:1 green:0.5 blue:0 alpha:1],
            [UIColor colorWithRed:1 green:0.75 blue:0 alpha:1],
            [UIColor colorWithRed:1 green:1 blue:0 alpha:1],
            [UIColor colorWithRed:0.75 green:1 blue:0 alpha:1],
            [UIColor colorWithRed:0.5 green:1 blue:0 alpha:1],
            [UIColor colorWithRed:0.25 green:1 blue:0 alpha:1],
            [UIColor colorWithRed:0 green:1 blue:0 alpha:1],
            [UIColor colorWithRed:0 green:1 blue:0.25 alpha:1],
            [UIColor colorWithRed:0 green:1 blue:0.5 alpha:1],
            [UIColor colorWithRed:0 green:1 blue:0.75 alpha:1],
            [UIColor colorWithRed:0 green:1 blue:1 alpha:1],
            [UIColor colorWithRed:0 green:0.75 blue:1 alpha:1],
            [UIColor colorWithRed:0 green:0.5 blue:1 alpha:1],
            [UIColor colorWithRed:0 green:0.25 blue:1 alpha:1],
            [UIColor colorWithRed:0 green:0 blue:1 alpha:1],
            [UIColor colorWithRed:0.5 green:0 blue:1 alpha:1],
            [UIColor colorWithRed:0.75 green:0 blue:1 alpha:1],
            [UIColor colorWithRed:1 green:0 blue:1 alpha:1],
            [UIColor colorWithRed:1 green:0 blue:0.75 alpha:1],
            [UIColor colorWithRed:1 green:0 blue:0.5 alpha:1],
            [UIColor colorWithRed:1 green:0 blue:0.25 alpha:1],
            ];
}

+ (NSString*)gameCompletedMessage
{
    NSArray* messageArray = @[
                              @"Well, that didn't take long at all.",
                              @"Wow, congrats. You're smarter than an asteriod",
                              ];
    
    return [messageArray objectAtIndex:arc4random() % [messageArray count]];
}

+ (NSString*)threeMoonsMessage
{
    NSArray* messageArray = @[
                              @"Way to go, you're the best! (NOT)",
                              @"SMILEYFACE",
                              @"17th times the charm they say!",
                              @"Three moons! Check the tide.",
                              @"How many moons does it take to change a light?", // longest possible on 6S
                              ];
    
    return [messageArray objectAtIndex:arc4random() % [messageArray count]];
}

+ (NSString*)twoMoonsMessage
{
    NSArray* messageArray = @[
                              @"Two moons! You are completely mediocre.",
                              @"Being average is almost above average!",
                              @"Wow, I'm stunned.",
                              @"Um. That was painful.",
                              @"Watch out, this person means business!",
                              ];
    
    return [messageArray objectAtIndex:arc4random() % [messageArray count]];
}

+ (NSString*)oneMoonsMessage
{
    NSArray* messageArray = @[
                              @"Of all the losers, you are the best!",
                              @"Well, at least you can move on.",
                              @"Hooray.  You can be taught!",
                              @"How many times did that take you?",
                              @"Um, I won't tell anyone if you don't.",
                              ];
    
    return [messageArray objectAtIndex:arc4random() % [messageArray count]];
}

+ (NSString*)zeroMoonsMessage
{
    NSArray* messageArray = @[
                              @"Wow, you really stink.",
                              @"I'm surprised you can turn on a phone.",
                              @"That was kind of embarrassing.",
                              @"Well, there's always next time... Maybe",
                              @"It's not you....\nit's me.",
                              ];
    
    return [messageArray objectAtIndex:arc4random() % [messageArray count]];
}

+ (UIColor*)colorFromHexString:(NSString*)hexString
{
    if ([hexString isEqualToString:@"------"])
    {
        return [UIColor clearColor];
    }
    
    unsigned rgbValue = 0;
    NSScanner* scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:0]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16) / 255.0 green:((rgbValue & 0xFF00) >> 8) / 255.0 blue:(rgbValue & 0xFF) / 255.0 alpha:1.0];
}


#pragma mark - Button Colors

+ (UIColor*)playButtonColor
{
    return [SPCYSettings colorFromHexString:@"BF247A"];
}

+ (UIColor*)menuButtonColor
{
    return [SPCYSettings colorFromHexString:@"035AA6"];
}

+ (UIColor*)settingsButtonColor
{
    return [SPCYSettings colorFromHexString:@"F62459"];
}

+ (UIColor*)restartButtonColor
{
    return [SPCYSettings colorFromHexString:@"41AEF2"];
}

+ (UIColor*)levelsButtonColor
{
    return [SPCYSettings colorFromHexString:@"F2D335"];
}

+ (UIColor*)quitButtonsColor
{
    return [SPCYSettings colorFromHexString:@"A68B03"];
}





@end
