//
//  MoonNode.m
//  spacey
//
//  Created by Chad Olsen on 11/22/15.
//  Copyright © 2015 Kraig. All rights reserved.
//

#import "MoonNode.h"
#import "MenuPlanet.h"

@interface MoonNode ()

@property (nonatomic) CGFloat previousDX;

@end

@implementation MoonNode

- (void)moveMoon
{
    
    if (self.previousDX > 0 && self.physicsBody.velocity.dx < 0)
    {
        self.zPosition = (self.parentPlanet.zPosition - 2.0);
    }
    else if (self.previousDX < 0 && self.physicsBody.velocity.dx > 0)
    {
        self.zPosition = (self.parentPlanet.zPosition + 2.0);
    }
    self.previousDX = self.physicsBody.velocity.dx;
//    NSLog([NSString stringWithFormat:@"dX : %f dY : %f", self.physicsBody.velocity.dx, self.physicsBody.velocity.dy]);
}

@end
