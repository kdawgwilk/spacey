//
//  Node.m
//  code_camp_2015
//
//  Created by Donald Timpson on 11/13/15.
//  Copyright © 2015 Kraig. All rights reserved.
//

#import "Node.h"
#import "Level.h"

@implementation Node

static double const marginPercent = .05;

+ (Node*)setupNodeWithSize:(CGSize)size type:(NodeType)type
{
    Node* newNode = [[Node alloc] init];
    newNode.type = [NSNumber numberWithInt:type];
    newNode.size = size;
    
    NSMutableArray* arrayOfTextures = [[NSMutableArray alloc] init];
    if (newNode.type.integerValue == kStart)
    {
        for (int i = 0; i <= 119; i++)
        {
            NSString* name = @"middle";
            NSString* fileName = [Node fileNameForName:name andIndex:i];
            [arrayOfTextures addObject:[SKTexture textureWithImageNamed:fileName]];
        }
    }
    else if (newNode.type.integerValue == kEnd)
    {
        for (int i = 0; i <= 119; i++)
        {
            NSString* name = @"end";
            NSString* fileName = [Node fileNameForName:name andIndex:i];
            [arrayOfTextures addObject:[SKTexture textureWithImageNamed:fileName]];
        }
    }
    else if (newNode.type.integerValue == kMiddle)
    {
        [newNode setTexture:[SKTexture textureWithImageNamed:@"middle_119.png"]];
//        for (int i = 0; i <= 119; i++)
//        {
//            NSString* name = @"middle";
//            NSString* fileName = [Node fileNameForName:name andIndex:i];
//            [arrayOfTextures addObject:[SKTexture textureWithImageNamed:fileName]];
//        }
    }
    else if (newNode.type.integerValue == kBlackHole)
    {
        for (int i = 0; i <= 34; i++)
        {
            NSString* name = @"node_blackhole";
            NSString* fileName = [Node fileNameForName:name andIndex:i];
            [arrayOfTextures addObject:[SKTexture textureWithImageNamed:fileName]];
        }
    }
    
    if ([arrayOfTextures count] > 0)
    {
        [newNode runAction:[SKAction repeatActionForever:[SKAction animateWithTextures:arrayOfTextures timePerFrame:0.125]]];
    }
    
    return newNode;
}

+ (float)randFloatBetween:(float)low and:(float)high
{
    float diff = high - low;
    return (((float) rand() / RAND_MAX) * diff) + low;
}

+ (NSString*)nameForType:(NodeType)type
{
    if (type == kStart)
    {
        return @"start";
    }
    if (type == kEnd)
    {
        return @"end";
    }
    if (type == kBlackHole)
    {
        return @"blackhole";
    }
    
    return @"middle";
}

+ (Node*)setupNodeWithDictionary:(NSDictionary*)nodeDict forLevel:(Level*)level
{
    Node* newNode = [[Node alloc] init];
    newNode.type = [nodeDict objectForKey:@"type"];
    newNode.x = [nodeDict objectForKey:@"x"];
    newNode.y = [nodeDict objectForKey:@"y"];
    
    NSMutableArray* arrayOfTextures = [[NSMutableArray alloc] init];
    if (newNode.type.integerValue == kStart)
    {
        [newNode setTexture:[SKTexture textureWithImageNamed:@"middle_001"]];
    }
    else if (newNode.type.integerValue == kMiddle)
    {
        [newNode setTexture:[SKTexture textureWithImageNamed:@"middle_001"]];
    }
    else if (newNode.type.integerValue == kEnd)
    {
        for (int i = 0; i <= 119; i++)
        {
            NSString* name = @"end";
            NSString* fileName = [Node fileNameForName:name andIndex:i];
            [arrayOfTextures addObject:[SKTexture textureWithImageNamed:fileName]];
        }
    }
    else if (newNode.type.integerValue == kBlackHole)
    {
//        for (int i = 0; i <= 34; i++)
//        {
//            NSString* name = @"node_blackhole";
//            NSString* fileName = [Node fileNameForName:name andIndex:i];
//            [arrayOfTextures addObject:[SKTexture textureWithImageNamed:fileName]];
//        }
        [newNode setTexture:[SKTexture textureWithImageNamed:@"node_blackhole_000"]];
    }
    
    if ([arrayOfTextures count] > 0)
    {
        [newNode runAction:[SKAction repeatActionForever:[SKAction animateWithTextures:arrayOfTextures timePerFrame:0.125]]];
    }
    
    if (newNode.type.integerValue == kBlackHole)
    {
        newNode.size = CGSizeMake(level.cellSize.width * 2.5, level.cellSize.height * 2.5);
    }
    else if (newNode.type.integerValue == kEnd)
    {
        newNode.size = CGSizeMake(level.cellSize.width * 1.25, level.cellSize.height * 1.25);
    }
    else
    {
        float percent = [Node randFloatBetween:0.50 and:0.75];
        float nodeWidth = level.cellSize.width * percent;
        float nodeHeight = level.cellSize.height * percent;
        newNode.size = CGSizeMake(nodeWidth, nodeHeight);
    }
    
    CGFloat deltaY = [[UIScreen mainScreen] bounds].size.height * 0.20;
    CGFloat deltaX = 6;
    newNode.position = CGPointMake(.5 * level.cellSize.width + newNode.x.doubleValue * level.cellSize.width + deltaX, .5 * level.cellSize.height + newNode.y.doubleValue * level.cellSize.height + deltaY);
    
    return newNode;
}

+ (SKShapeNode*)planetCircleBehindNodeWithEnd:(Node*)end
{
    CGSize circleSize = CGSizeMake(end.size.width * 0.9, end.size.height * 0.9);
    CGRect circle = CGRectMake(0, 0, circleSize.width, circleSize.height);
    SKShapeNode *shapeNode = [[SKShapeNode alloc] init];
    shapeNode.path = [UIBezierPath bezierPathWithOvalInRect:circle].CGPath;
    shapeNode.fillColor = [SKColor colorWithRed:255.0/255.0 green:229.0/255.0 blue:0.0/255.5 alpha:1.0];
    shapeNode.lineWidth = 0;
    
    return shapeNode;
}

+ (NSString*)fileNameForName:(NSString*)name andIndex:(int)i
{
    NSString* imageName = [[NSString alloc] init];
    
    if (i < 10)
    {
        imageName = [NSString stringWithFormat:@"%@_00%d", name, i];
    }
    else if (i < 100)
    {
        imageName = [NSString stringWithFormat:@"%@_0%d", name, i];
    }
    else
    {
        imageName = [NSString stringWithFormat:@"%@_%d", name, i];
    }
    
    return imageName;
}

@end
