//
//  WormHole.m
//  code_camp_2015
//
//  Created by Donald Timpson on 11/13/15.
//  Copyright © 2015 Kraig. All rights reserved.
//

#import "WormHole.h"
#import "Level.h"

@implementation WormHole

static double const marginPercent = .05;

+ (WormHole*)setupWormHoleWithDictionary:(NSDictionary*)nodeDict forLevel:(Level*)level
{
    WormHole* wormHole = [[WormHole alloc] init];
    wormHole.type = [nodeDict objectForKey:@"type"];
    wormHole.x = [nodeDict objectForKey:@"x"];
    wormHole.y = [nodeDict objectForKey:@"y"];
    wormHole.name = @"wormhole";
    wormHole.linkId = [nodeDict objectForKey:@"linkId"];
    
    NSMutableArray* arrayOfTextures = [[NSMutableArray alloc] init];
    for (int i = 0; i <= 20; i++)
    {
        NSString* name = @"wormhole";
        NSString* fileName = [WormHole fileNameForName:name andIndex:i];
        [arrayOfTextures addObject:[SKTexture textureWithImageNamed:fileName]];
    }
    
    [wormHole runAction:[SKAction repeatActionForever:[SKAction animateWithTextures:arrayOfTextures timePerFrame:0.125]]];
    
    CGFloat gridSquareWidth = level.width.doubleValue / level.numberOfColumns.doubleValue;
    CGFloat gridSquareHeight = level.height.doubleValue / level.numberOfRows.doubleValue;
    
    wormHole.size = CGSizeMake(gridSquareWidth - (2 * (marginPercent * gridSquareWidth)), gridSquareHeight - (2 * (marginPercent * gridSquareHeight)));
    CGFloat deltaY = [[UIScreen mainScreen] bounds].size.height * 0.20;
    CGFloat deltaX = 6;
    wormHole.position = CGPointMake(.5 * gridSquareWidth + wormHole.x.doubleValue * gridSquareWidth + deltaX, .5 * gridSquareHeight + wormHole.y.doubleValue * gridSquareHeight + deltaY);
    
    return wormHole;
}

+ (NSString*)fileNameForName:(NSString*)name andIndex:(int)i
{
    NSString* imageName = [[NSString alloc] init];
    
    if (i < 10)
    {
        imageName = [NSString stringWithFormat:@"%@_00%d", name, i];
    }
    else if (i < 100)
    {
        imageName = [NSString stringWithFormat:@"%@_0%d", name, i];
    }
    else
    {
        imageName = [NSString stringWithFormat:@"%@_%d", name, i];
    }
    
    return imageName;
}

@end
