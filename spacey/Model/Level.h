//
//  Level.h
//  code_camp_2015
//
//  Created by Donald Timpson on 11/13/15.
//  Copyright © 2015 Kraig. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Node.h"

@interface Level : NSObject

@property (strong, nonatomic) Node* start;
@property (strong, nonatomic) Node* end;
@property (strong, nonatomic) NSArray* nodes;
@property (strong, nonatomic) NSArray* wormHoles;
@property (strong, nonatomic) NSNumber* numberOfRows;
@property (strong, nonatomic) NSNumber* numberOfColumns;
@property (strong, nonatomic) NSNumber* width;
@property (strong, nonatomic) NSNumber* height;
@property (strong, nonatomic) NSNumber* moves;
@property CGSize cellSize;

+ (Level*)getLevelWithName:(NSNumber*)levelNumber width:(NSNumber*)width height:(NSNumber*)height;

+ (NSNumber*)getScoreForLevel:(Level*)level withTime:(NSNumber*)time moves:(NSNumber*)moves;

+ (NSInteger)getTotalNumberOfLevels;

@end
