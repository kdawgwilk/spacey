//
//  WormHole.h
//  code_camp_2015
//
//  Created by Donald Timpson on 11/13/15.
//  Copyright © 2015 Kraig. All rights reserved.
//

#import "Node.h"
@class Level;

@interface WormHole : Node

@property (strong, nonatomic) Node* partnerNode;
@property (strong, nonatomic) NSNumber* linkId;

+ (WormHole*)setupWormHoleWithDictionary:(NSDictionary*)nodeDict forLevel:(Level*)level;

@end
