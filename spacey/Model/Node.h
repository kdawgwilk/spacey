//
//  Node.h
//  code_camp_2015
//
//  Created by Donald Timpson on 11/13/15.
//  Copyright © 2015 Kraig. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
@class Level;

typedef enum {
    kStart = 10,
    kEnd = 20,
    kMiddle = 30,
    kWormHole = 40,
    kBlackHole = 50
}NodeType;


@interface Node : SKSpriteNode

@property (strong, nonatomic) NSNumber* type;
@property (strong, nonatomic) NSNumber* x;
@property (strong, nonatomic) NSNumber* y;

+ (Node*)setupNodeWithDictionary:(NSDictionary*)nodeDict forLevel:(Level*)level;

+ (Node*)setupNodeWithSize:(CGSize)size type:(NodeType)type;

@end
