//
//  soundController.h
//  code_camp_2015
//
//  Created by Chad Olsen on 11/14/15.
//  Copyright © 2015 Kraig. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import <Foundation/Foundation.h>

@interface SoundController : NSObject

+ (void)playBackgroundMusicForScene:(SKScene*)scene;

+ (void)playButtonSelectSoundForScene:(SKScene*)scene;

+ (void)playShipMoveSoundForScene:(SKScene*)scene;

+ (void)playFailSoundForScene:(SKScene*)scene;

@end
