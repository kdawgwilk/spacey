//
//  soundController.m
//  code_camp_2015
//
//  Created by Chad Olsen on 11/14/15.
//  Copyright © 2015 Kraig. All rights reserved.
//

#import "SoundController.h"

@implementation SoundController

+ (void)playBackgroundMusicForScene:(SKScene*)scene
{
    [scene runAction:[SKAction playSoundFileNamed:@"SpacerBackground.mp3" waitForCompletion:NO]];
}

+ (void)playButtonSelectSoundForScene:(SKScene*)scene
{
    [scene runAction:[SKAction playSoundFileNamed:@"button.wav" waitForCompletion:NO]];
}

+ (void)playShipMoveSoundForScene:(SKScene*)scene
{
    [scene runAction:[SKAction playSoundFileNamed:@"move.mp3" waitForCompletion:NO]];
}

+ (void)playFailSoundForScene:(SKScene *)scene
{
    [scene runAction:[SKAction playSoundFileNamed:@"lose.mp3" waitForCompletion:NO]];
}

+ (void)playWinSoundForScene:(SKScene*)scene
{
    [scene runAction:[SKAction playSoundFileNamed:@"win.wav" waitForCompletion:NO]];
}

@end
