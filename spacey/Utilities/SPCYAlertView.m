//
//  SPCYAlertView.m
//  spacey
//
//  Created by Kraig Wastlund on 11/21/15.
//  Copyright © 2015 Kraig. All rights reserved.
//

#import "SPCYAlertView.h"
#import "Level.h"
#import "SPCYSettings.h"
#import "SPCYButton.h"

@implementation SPCYAlertView

- (instancetype)initWithLevelNumber:(NSNumber*)levelNumber worldsBest:(NSNumber*)worldsBest currentTime:(NSNumber*)currentTime bestPersonalTime:(NSNumber*)bestPersonalTime levelScore:(NSNumber*)score andGameCode:(FinishedGameCode)gameCode
{
    self = [super init];
    
    if (self)
    {
        BOOL moreLevels = levelNumber.integerValue < [Level getTotalNumberOfLevels];
        
        // times
        // current time
        double timePassed_ms = currentTime.integerValue * -1000.0;
        float currentSeconds = timePassed_ms / 1000.0;
        float currentMinutes = currentSeconds / 60.0;
        
        // personal best
        timePassed_ms = bestPersonalTime.integerValue * -1000.0;
        float personalBestSeconds = timePassed_ms / 1000.0;
        float personalBestMinutes = personalBestSeconds / 60.0;
        
        // world's best
        timePassed_ms = worldsBest.integerValue * -1000.0;
        float worldsBestSeconds = timePassed_ms / 1000.0;
        float worldsBestMinutes = worldsBestSeconds / 60.0;
        
        // locations
        int screenWidth = [UIScreen mainScreen].bounds.size.width;
        int screenHeight = [UIScreen mainScreen].bounds.size.height;
        int x = 0;
        int y = x;
        int width = screenWidth;
        int height = screenHeight;
        int center = width / 2;
        int verticalPadding = screenHeight == 480 ? screenHeight * 0.02 : screenHeight * 0.04; // if i'm a iphone 4s then use less padding
        
        [self setFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
        [self setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:1.0]];
        
        CGRect viewFrame = CGRectMake(x, y, width, height);
        // this uiview is where everything lives, it gets put onto self (this was just in case i wanted it to be slightly smaller than the overall view with a different background color)
        UIView* alertView = [[UIView alloc] initWithFrame:viewFrame];
        
        int elementY = verticalPadding * 2; // this is where the y starts at the top
        
        // level title
        UILabel* titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, elementY, alertView.frame.size.width, 40)];
        [titleLabel setFont:[UIFont systemFontOfSize:24 weight:UIFontWeightLight]];
        [titleLabel setTextColor:[UIColor whiteColor]];
        [titleLabel setTextAlignment:NSTextAlignmentCenter];
        [titleLabel setText:[NSString stringWithFormat:@"Planet %03ld", (long)levelNumber.integerValue]];
        [alertView addSubview:titleLabel];
        
        elementY += titleLabel.frame.size.height;
        
        // game message
        NSString* message;
        if (gameCode == kGameWon && moreLevels == NO)
        {
            message = [SPCYSettings gameCompletedMessage];
        }
        else if (gameCode == kGameQuit)
        {
            message = @"You lose, because you quit.";
        }
        else if (gameCode == kGameLost)
        {
            message = [SPCYSettings zeroMoonsMessage];
        }
        else
        {
            if (score.integerValue > 99)
            {
                message = [SPCYSettings threeMoonsMessage];
            }
            else if (score.integerValue > 50)
            {
                message = [SPCYSettings twoMoonsMessage];
            }
            else
            {
                message = [SPCYSettings oneMoonsMessage];
            }
        }
        
        UILabel* messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, elementY, alertView.frame.size.width - 30, 60)];
        [messageLabel setFont:[UIFont systemFontOfSize:20 weight:UIFontWeightUltraLight]];
        [messageLabel setTextColor:[UIColor whiteColor]];
        [messageLabel setTextAlignment:NSTextAlignmentCenter];
        [messageLabel setText:message];
        [messageLabel setNumberOfLines:0];
        [messageLabel setLineBreakMode:NSLineBreakByWordWrapping];
        [alertView addSubview:messageLabel];
        
        elementY += messageLabel.frame.size.height + verticalPadding;
        
        // level representation
        int imageWidth = width * 0.25;
        UIImageView* levelImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"end_004.png"]];
        [levelImageView setFrame:CGRectMake(center - imageWidth / 2, elementY, imageWidth, imageWidth)];
        [alertView addSubview:levelImageView];
        
        elementY += levelImageView.frame.size.height + verticalPadding;
        
        // World's Best title
        UILabel* worldTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, elementY, alertView.frame.size.width, 24)];
        [worldTitle setFont:[UIFont systemFontOfSize:20 weight:UIFontWeightUltraLight]];
        [worldTitle setTextColor:[UIColor whiteColor]];
        [worldTitle setTextAlignment:NSTextAlignmentCenter];
        [worldTitle setText:@"World's Best"];
        [alertView addSubview:worldTitle];
        
        elementY += worldTitle.frame.size.height;
        
        // World's Best value
        UILabel* worldValue = [[UILabel alloc] initWithFrame:CGRectMake(0, elementY, alertView.frame.size.width, 24)];
        [worldValue setFont:[UIFont systemFontOfSize:20 weight:UIFontWeightLight]];
        [worldValue setTextColor:[UIColor whiteColor]];
        [worldValue setTextAlignment:NSTextAlignmentCenter];
        [worldValue setText:[NSString stringWithFormat:@"%02ld:%02ld", (long)worldsBestMinutes, (long)worldsBestSeconds]];
        [alertView addSubview:worldValue];
        
        elementY += worldValue.frame.size.height + verticalPadding;
        
        // current time title
        UILabel* currentTimeTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, elementY, alertView.frame.size.width, 24)];
        [currentTimeTitle setFont:[UIFont systemFontOfSize:20 weight:UIFontWeightUltraLight]];
        [currentTimeTitle setTextColor:[UIColor whiteColor]];
        [currentTimeTitle setTextAlignment:NSTextAlignmentCenter];
        [currentTimeTitle setText:@"Time"];
        [alertView addSubview:currentTimeTitle];
        
        elementY += worldTitle.frame.size.height;
        
        // current time value
        UILabel* currentTimeValue = [[UILabel alloc] initWithFrame:CGRectMake(0, elementY, alertView.frame.size.width, 24)];
        [currentTimeValue setFont:[UIFont systemFontOfSize:20 weight:UIFontWeightLight]];
        [currentTimeValue setTextColor:[UIColor whiteColor]];
        [currentTimeValue setTextAlignment:NSTextAlignmentCenter];
        NSString* timeString = [NSString stringWithFormat:@"%02ld:%02ld", (long)currentMinutes, (long)currentSeconds];
        if (gameCode == kGameQuit)
        {
            timeString = @". . .";
        }
        [currentTimeValue setText:timeString];
        [alertView addSubview:currentTimeValue];
        
        elementY += worldValue.frame.size.height + verticalPadding;
        
        // personal best title
        UILabel* personalBestTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, elementY, alertView.frame.size.width, 24)];
        [personalBestTitle setFont:[UIFont systemFontOfSize:20 weight:UIFontWeightUltraLight]];
        [personalBestTitle setTextColor:[UIColor whiteColor]];
        [personalBestTitle setTextAlignment:NSTextAlignmentCenter];
        [personalBestTitle setText:@"Best Time"];
        [alertView addSubview:personalBestTitle];
        
        elementY += worldTitle.frame.size.height;
        
        // personal best value
        UILabel* personalBestValue = [[UILabel alloc] initWithFrame:CGRectMake(0, elementY, alertView.frame.size.width, 24)];
        [personalBestValue setFont:[UIFont systemFontOfSize:20 weight:UIFontWeightLight]];
        [personalBestValue setTextColor:[UIColor whiteColor]];
        [personalBestValue setTextAlignment:NSTextAlignmentCenter];
        [personalBestValue setText:[NSString stringWithFormat:@"%02ld:%02ld", (long)personalBestMinutes, (long)personalBestSeconds]];
        [alertView addSubview:personalBestValue];
        
        elementY += worldValue.frame.size.height + verticalPadding;
        
        // buttons are pinned to the bottom of the view
        // buttons
        int edgeMargin = 10;
        int horizontalPadding = 8;
        int buttonHeight = 50;
        int buttonX = edgeMargin;
        int buttonY = height - buttonHeight - edgeMargin;
        float middleButtonWidth = alertView.frame.size.width * 0.45;
        float sideButtonWidth = ((alertView.frame.size.width - middleButtonWidth) - (edgeMargin * 2 + horizontalPadding * 2)) / 2;
        
        // button 1 (left button)
        CGRect buttonFrame = CGRectMake(buttonX, buttonY, sideButtonWidth, buttonHeight);
        SPCYButton* button1 = [SPCYButton buttonWithFrame:buttonFrame overlayImage:[UIImage imageNamed:@"menu_overlay"] andBackgroundColor:[SPCYSettings menuButtonColor]];
        [button1 addTarget:self action:@selector(quitButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        [alertView addSubview:button1];
        
        buttonX += sideButtonWidth + horizontalPadding;
        
        // button 2 (middle button)
        buttonFrame = CGRectMake(buttonX, buttonY, middleButtonWidth, buttonHeight);
        SPCYButton* button2 = [SPCYButton buttonWithFrame:buttonFrame overlayImage:[UIImage imageNamed:@"play_overlay"] andBackgroundColor:[SPCYSettings playButtonColor]];
        [button2 addTarget:self action:@selector(playNextButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        [alertView addSubview:button2];
        if (gameCode == kGameWon && moreLevels)
        {
            [button2 setEnabled:YES];
        }
        else
        {
            [button2 setEnabled:NO];
        }
        
        buttonX += middleButtonWidth + horizontalPadding;
        
        // button 3 (right button)
        buttonFrame = CGRectMake(buttonX, buttonY, sideButtonWidth, buttonHeight);
        SPCYButton* button3 = [SPCYButton buttonWithFrame:buttonFrame overlayImage:[UIImage imageNamed:@"restart_overlay"] andBackgroundColor:[SPCYSettings restartButtonColor]];
        [button3 addTarget:self action:@selector(replayButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        [alertView addSubview:button3];
        
        [self addSubview:alertView];
        [self bringSubviewToFront:alertView];
    }
    
    return self;
}

- (void)quitButtonPressed
{
    [self.alertDelegate alertView:self didSelectWithIndex:kTypeRight];
}

- (void)replayButtonPressed
{
    [self.alertDelegate alertView:self didSelectWithIndex:kTypeLeft];
}

- (void)playNextButtonPressed
{
    [self.alertDelegate alertView:self didSelectWithIndex:kTypeMiddle];
}

@end
