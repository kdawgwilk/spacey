//
//  SPCYAlertView.h
//  spacey
//
//  Created by Kraig Wastlund on 11/21/15.
//  Copyright © 2015 Kraig. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameScene.h"

typedef enum{
    kTypeLeft,
    kTypeMiddle,
    kTypeRight,
}AlertButtonType;

@class SPCYAlertView;

@protocol SPCYAlertDelegate <NSObject>

@optional

- (void)alertView:(SPCYAlertView*)view didSelectWithIndex:(AlertButtonType)buttonType;

@end

@interface SPCYAlertView : UIView

@property (nonatomic, assign) id<SPCYAlertDelegate> alertDelegate;

- (instancetype)initWithLevelNumber:(NSNumber*)levelNumber worldsBest:(NSNumber*)worldsBest currentTime:(NSNumber*)currentTime bestPersonalTime:(NSNumber*)bestPersonalTime levelScore:(NSNumber*)score andGameCode:(FinishedGameCode)gameCode;

@end
