//
//  SPCYButton.h
//  spacey
//
//  Created by Kraig Wastlund on 11/24/15.
//  Copyright © 2015 Kraig. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPCYButton : UIButton

+ (instancetype)buttonWithFrame:(CGRect)frame overlayImage:(UIImage*)overlayImage andBackgroundColor:(UIColor*)backgroundColor;

@end
