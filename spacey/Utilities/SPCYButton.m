//
//  SPCYButton.m
//  spacey
//
//  Created by Kraig Wastlund on 11/24/15.
//  Copyright © 2015 Kraig. All rights reserved.
//

#import "SPCYButton.h"

@implementation SPCYButton

+ (instancetype)buttonWithFrame:(CGRect)frame overlayImage:(UIImage*)overlayImage andBackgroundColor:(UIColor*)backgroundColor
{
    SPCYButton* button = [[SPCYButton alloc] init];
    UIColor* disabledColor = [UIColor grayColor];
    UIImage* disabledImage = [button disabledVersionOfImage:overlayImage];
    
    [button setFrame:frame];
    [button setBackgroundImage:[button backgroundImageWithColor:backgroundColor overlayImage:overlayImage andFrame:frame] forState:UIControlStateNormal];
    [button setBackgroundImage:[button backgroundImageWithColor:disabledColor overlayImage:disabledImage andFrame:frame] forState:UIControlStateDisabled];
    
    return button;
}

- (UIImage*)disabledVersionOfImage:(UIImage*)image
{
    UIGraphicsBeginImageContextWithOptions (image.size, NO, [[UIScreen mainScreen] scale]); // for correct resolution on retina, thanks @MobileVet
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextTranslateCTM(context, 0, image.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    
    CGRect rect = CGRectMake(0, 0, image.size.width, image.size.height);
    
    // draw alpha-mask
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    CGContextDrawImage(context, rect, image.CGImage);
    
    // draw tint color, preserving alpha values of original image
    CGContextSetBlendMode(context, kCGBlendModeSourceIn);
    [[UIColor darkGrayColor] setFill];
    CGContextFillRect(context, rect);
    
    UIImage *coloredImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return coloredImage;
}

- (UIImage*)backgroundImageWithColor:(UIColor*)color overlayImage:(UIImage*)overlayImage andFrame:(CGRect)frame
{
    float radius = 0.15 * frame.size.height;
    
    // front color
    UIView* foregroundView = [[UIView alloc] init];
    [foregroundView setFrame:CGRectMake(0, 0, frame.size.width, frame.size.height - 2)];
    [foregroundView setBackgroundColor:color];
    foregroundView.clipsToBounds = YES;
    foregroundView.layer.cornerRadius = radius;
    
    // image view button icon 200 X 300
    UIImageView* overlayImageView = [[UIImageView alloc] init];
    [overlayImageView setFrame:CGRectMake((foregroundView.frame.size.width - foregroundView.frame.size.height) / 2, 0, foregroundView.frame.size.height, foregroundView.frame.size.height)];
    [overlayImageView setImage:overlayImage];
    [foregroundView addSubview:overlayImageView];
    
    // semi transparent background view that is meant as an overlay for the background color
    UIView* backgroundView = [[UIView alloc] init];
    [backgroundView setFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
    [backgroundView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5]];
    backgroundView.clipsToBounds = YES;
    backgroundView.layer.cornerRadius = radius;
    
    // background color that is the same as front color with the semi transparent black on top
    UIView* backgroundExtremeView = [[UIView alloc] init];
    [backgroundExtremeView setFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
    [backgroundExtremeView setBackgroundColor:color];
    backgroundExtremeView.clipsToBounds = YES;
    backgroundExtremeView.layer.cornerRadius = radius;
    
    // put it all together
    UIView* totalView = [[UIView alloc] initWithFrame:frame];
    [totalView addSubview:backgroundExtremeView];
    [totalView addSubview:backgroundView];
    [totalView addSubview:foregroundView];
    
    // turn it into an image
    UIGraphicsBeginImageContextWithOptions(frame.size, NO, 0.0);
    [totalView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}


@end
