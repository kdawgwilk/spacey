//
//  LevelMenu.m
//  code_camp_2015
//
//  Created by Chad Olsen on 11/13/15.
//  Copyright © 2015 Kraig. All rights reserved.
//

#import "LevelMenu.h"
#import "Level.h"
#import "Player.h"
#import "MenuPlanet.h"
#import "SoundController.h"

@interface LevelMenu ()

@property (nonatomic, strong) SKSpriteNode* background;
@property (nonatomic, strong) SKSpriteNode* backButton;
@property (nonatomic, strong) NSMutableArray* levelButtonArray;
@property (nonatomic, strong) NSArray* colorsArray;
@property (nonatomic, strong) SKNode* fullView;
@property (nonatomic) BOOL isSwiping;

@property (nonatomic) CGFloat btnHeight;
@property (nonatomic) CGFloat btnWidth;
@property (nonatomic) CGFloat spacing;


@end

@implementation LevelMenu

-(void)didMoveToView:(SKView *)view
{
    self.btnHeight = 50;
    self.btnWidth = 50;
    self.spacing = 75;
    self.levelButtonArray = [[NSMutableArray alloc] init];
    self.colorsArray = [NSArray arrayWithObjects:
                        [SKColor redColor],
                        [SKColor yellowColor],
                        [SKColor magentaColor],
                        [SKColor cyanColor],
                        [SKColor brownColor],
                        [SKColor orangeColor],
                        [SKColor purpleColor],
                        [SKColor blackColor],
                        [SKColor blueColor],
                        nil];
    
    CGFloat yOffset = self.btnHeight + 10;
    NSInteger count = 1;
    
    
    self.background = [SKSpriteNode spriteNodeWithImageNamed:@"background"];
    self.background.size = self.frame.size;
    self.background.position = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
    self.background.zPosition = 0;
    [self addChild:self.background];
    
    self.fullView = [[SKNode alloc] init];
    self.fullView.position = CGPointMake(0, 0);
    [self addChild:self.fullView];
    
    NSInteger numberOfLevels = [Level getTotalNumberOfLevels];
    CGFloat xOffset = CGRectGetMidX(self.frame) / 2 * .66;
    
    for (int i = 1; i <= numberOfLevels; i++)
    {
        
        MenuPlanet* levelButton = [[MenuPlanet alloc] init];
        
        if (count%2 == 0)
        {
            levelButton.position = CGPointMake(CGRectGetMidX(self.frame) - xOffset, CGRectGetMinY(self.frame) + yOffset);
        }
        else
        {
            levelButton.position = CGPointMake(CGRectGetMidX(self.frame) + xOffset, CGRectGetMinY(self.frame) + yOffset);
        }

        levelButton.name = [NSString stringWithFormat:@"%d", i];
        levelButton.zPosition = 3;
        levelButton.worldColor = [self.colorsArray objectAtIndex:i % self.colorsArray.count];
        Player* player = [Player getPlayerWithCellSize:CGSizeMake(0, 0)];
        if (i > [player.currentLevel integerValue])
        {
            levelButton.isDark = YES;
        }
        else
        {
            levelButton.isDark = NO;
        }

        
        //        [self drawPlanetBehindEndAtPositon:levelButton.position inColor:[self.colorsArray objectAtIndex:(count % self.colorsArray.count)] isDark:darken];

        
        [levelButton setupWorldNode];
        
        
        [self.fullView addChild:levelButton];
        [self.levelButtonArray addObject:levelButton];
        
        yOffset += self.btnHeight + self.spacing;
        
        if (i != numberOfLevels - 1)
        {
            SKSpriteNode* path = [SKSpriteNode spriteNodeWithImageNamed:@"level_path"];
            path.size = CGSizeMake(xOffset * 2, self.btnHeight + self.spacing);
            path.position = CGPointMake(CGRectGetMidX(self.frame), yOffset - (self.btnHeight + self.spacing)/2);
            
            if (count%2 == 0)
            {
                path.yScale = -1.0;
            }
            path.zPosition = 2;
            [self.fullView addChild:path];
        }
        
        count++;
        
        
    }
    
    //Add button 1
    self.backButton = [SKSpriteNode spriteNodeWithImageNamed:@"back"];
    self.backButton.size = CGSizeMake(40, 43);
    self.backButton.position = CGPointMake(CGRectGetMinX(self.frame) + 30, CGRectGetMaxY(self.frame) - (self.btnHeight/2) - 10);
    self.backButton.zPosition = 2;
    [self addChild:self.backButton];
    
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    for (UITouch *touch in touches) {
        
        CGPoint location = [touch locationInNode:self.fullView];
        
        BOOL touchedButton = NO;
        
        if ([[self nodeAtPoint:location] isEqualToNode:self.backButton])
        {
            [self runAction:[SKAction playSoundFileNamed:@"click.mp3" waitForCompletion:NO]];
            [self.levelMenuViewController backSelected];
            touchedButton = YES;
            [self removeActionsFromView];
        }
        
        for (MenuPlanet* levelNode in self.levelButtonArray)
        {
            if ([levelNode containsPoint:location])
            {
                touchedButton = YES;
                NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
                f.numberStyle = NSNumberFormatterDecimalStyle;
                NSNumber* levelNumber = [f numberFromString:levelNode.name];
                Player* player = [Player getPlayerWithCellSize:(CGSizeMake(0, 0))];
                if ([levelNumber isEqualToNumber:@1] || levelNumber <= player.currentLevel)
                {
                    [self runAction:[SKAction playSoundFileNamed:@"click.mp3" waitForCompletion:NO]];
                    [self.levelMenuViewController levelSelected:levelNumber];
                    [self removeActionsFromView];
                }
                break;
            }
        }
        if (!touchedButton)
        {
            self.isSwiping = YES;
        }
    }
}

-(void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    if (self.isSwiping) {
        // 2 Get touch location
        UITouch* touch = [touches anyObject];
        CGPoint touchLocation = [touch locationInNode:self];
        CGPoint previousLocation = [touch previousLocationInNode:self];
        
        CGFloat pageY = self.fullView.position.y + (touchLocation.y - previousLocation.y);
        
        if (pageY > 0)
        {
            pageY = 0;
        }
        
        if (-pageY > ((self.btnHeight + self.spacing) * ([Level getTotalNumberOfLevels] +1 ) - self.frame.size.height))
        {
            pageY = self.fullView.position.y;
        }
        
        //        self.fullView = MAX(paddleX, paddle.size.width/2);
        //        paddleX = MIN(paddleX, self.size.width - paddle.size.width/2);
        //        // 6 Update position of paddle
        //        paddle.position = CGPointMake(paddleX, paddle.position.y);
        self.fullView.position = CGPointMake(self.fullView.position.x, pageY);
    }
}

-(void)update:(CFTimeInterval)currentTime
{
    /* Called before each frame is rendered */
}

-(void)didSimulatePhysics
{
    for (MenuPlanet* i in self.levelButtonArray)
    {
        [i movePlanetNode];
    }
}

- (void)removeActionsFromView
{
    for (MenuPlanet* world in self.levelButtonArray)
    {
        [world removeMenuPlanet];
    }
}

@end
