//
//  LevelMenuViewController.m
//  code_camp_2015
//
//  Created by Chad Olsen on 11/13/15.
//  Copyright © 2015 Kraig. All rights reserved.
//

#import "LevelMenuViewController.h"
#import "GameViewController.h"
#import "LevelMenu.h"

@implementation SKScene (Unarchive)

+ (instancetype)unarchiveFromFile:(NSString *)file {
    /* Retrieve scene file path from the application bundle */
    NSString *nodePath = [[NSBundle mainBundle] pathForResource:file ofType:@"sks"];
    /* Unarchive the file to an SKScene object */
    NSData *data = [NSData dataWithContentsOfFile:nodePath
                                          options:NSDataReadingMappedIfSafe
                                            error:nil];
    NSKeyedUnarchiver *arch = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
    [arch setClass:self forClassName:@"SKScene"];
    SKScene *scene = [arch decodeObjectForKey:NSKeyedArchiveRootObjectKey];
    [arch finishDecoding];
    
    return scene;
}

@end

@interface LevelMenuViewController ()

@property (nonatomic, strong) NSNumber* levelSelected;
@property (nonatomic, weak) LevelMenu* levelMenu;
@property (nonatomic, strong) SKView* skView;

@end

@implementation LevelMenuViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Configure the view.
    self.skView = (SKView *)self.view;
    /* Sprite Kit applies additional optimizations to improve rendering performance */
    self.skView.ignoresSiblingOrder = YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // Create and configure the scene.
    self.levelMenu = [LevelMenu unarchiveFromFile:@"LevelMenuScene"];
    [self.levelMenu setSize:self.view.frame.size];
    self.levelMenu.scaleMode = SKSceneScaleModeResizeFill;
    self.levelMenu.levelMenuViewController = self;
    
    // Present the scene.
    [self.skView presentScene:self.levelMenu];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

-(void)backSelected
{
    [self.levelMenu removeAllActions];
    [self.levelMenu removeAllChildren];
    [self performSegueWithIdentifier:@"Back To Menu" sender:nil];
}

-(void)levelSelected:(NSNumber*)levelNumber
{
    self.levelSelected = levelNumber;
    [self performSegueWithIdentifier:@"Levels To Game" sender:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Levels To Game"])
    {
        GameViewController* gameViewController = segue.destinationViewController;
        gameViewController.levelNumber = self.levelSelected;
        [self performSelector:@selector(endScene) withObject:nil afterDelay:1.0];
    }
}

- (void)endScene // this is supposed to clean up the skview for performance
{
    [self.levelMenu removeAllActions];
    [self.levelMenu removeAllChildren];
    [self.levelMenu removeFromParent];
    [self.skView presentScene:nil];
    [self.skView removeFromSuperview];
}

@end
