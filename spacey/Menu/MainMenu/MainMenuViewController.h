//
//  MainMenuViewController.h
//  code_camp_2015
//
//  Created by Chad Olsen on 11/13/15.
//  Copyright © 2015 Kraig. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>

@class MainMenu;

@interface MainMenuViewController : UIViewController

@property (nonatomic, weak) MainMenu* mainMenu;

- (void)playSelected;
- (void)levelsSelected;
- (void)settingsSelected;
- (void)aboutSelected;

@end
