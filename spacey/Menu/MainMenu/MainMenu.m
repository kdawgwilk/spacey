//
//  MainMenu.m
//  code_camp_2015
//
//  Created by Chad Olsen on 11/13/15.
//  Copyright © 2015 Kraig. All rights reserved.
//

#import "MainMenu.h"
#import "SoundController.h"
#import "AppDelegate.h"
@import AVFoundation;

@interface MainMenu ()

@property (nonatomic, strong) SKSpriteNode* background;
@property (nonatomic, strong) SKSpriteNode* playButton;
@property (nonatomic, strong) SKSpriteNode* levelsButton;
@property (nonatomic, strong) SKSpriteNode* settingsButton;
@property (nonatomic, strong) SKSpriteNode* ship;
@property (nonatomic, strong) NSArray* shipFlyingFrames;
@property (nonatomic) AVAudioPlayer* clickSoundPlayer;

@end

@implementation MainMenu

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    // this gets called first
    // load sound, ai, unit stats that don't change
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        // do stuff here
        self.size = [UIScreen mainScreen].bounds.size;
        self.scaleMode = SKSceneScaleModeResizeFill;
        
        SKSpriteNode* background = [SKSpriteNode spriteNodeWithImageNamed:@"background"];
        background.position = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
        background.size = self.size;
        [self addChild:background];
    }
    
    return self;
}

-(void)didMoveToView:(SKView *)view
{
    [self setupShipFlyingFrames];
    
    NSError *error;
    NSURL * clickURL = [[NSBundle mainBundle] URLForResource:@"click" withExtension:@"mp3"];
    self.clickSoundPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:clickURL error:&error];
    [self.clickSoundPlayer prepareToPlay];
//    [self.clickSoundPlayer setVolume:1.0];

    CGFloat shipHW = self.frame.size.width;
    
    SKTexture* firstFrame = [self.shipFlyingFrames objectAtIndex:0];
    self.ship = [SKSpriteNode spriteNodeWithTexture:firstFrame];
    self.ship.size = CGSizeMake(shipHW, shipHW);
    self.ship.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame) + shipHW * .15);
    self.ship.zPosition = 1;
    [self addChild:self.ship];
    [self flyShip];
    
    
    SKSpriteNode *title = [SKSpriteNode spriteNodeWithImageNamed:@"title"];
    title.size = CGSizeMake(self.frame.size.width, self.frame.size.width * .31);
    title.position = CGPointMake(CGRectGetMidX(self.frame), self.ship.position.y + shipHW* .40);
    title.zPosition = 1;
    [self addChild:title];
}

-(void)willMoveFromView:(SKView *)view
{
    [self.ship removeAllActions];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    
//    for (UITouch *touch in touches) {
//        
//        CGPoint location = [touch locationInNode:self];
//        
//        if ([[self nodeAtPoint:location] isEqualToNode:self.playButton])
//        {
//            [self runAction:self.clickSoundAction];
//            [self.mainMenuViewController playSelected];
//        }
//        else if ([[self nodeAtPoint:location] isEqualToNode:self.levelsButton])
//        {
//            [self runAction:self.clickSoundAction];
//            [self.mainMenuViewController levelsSelected];
//        }
//        else if ([[self nodeAtPoint:location] isEqualToNode:self.settingsButton])
//        {
//            [self runAction:self.clickSoundAction];
//            [self.mainMenuViewController settingsSelected];
//        }
//    }
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    for (UITouch *touch in touches) {
        
        CGPoint location = [touch locationInNode:self];
        
        if ([[self nodeAtPoint:location] isEqualToNode:self.playButton])
        {
            [self.clickSoundPlayer play];
            [self.mainMenuViewController playSelected];
            [self.ship removeAllActions];
        }
        else if ([[self nodeAtPoint:location] isEqualToNode:self.levelsButton])
        {
            [self.clickSoundPlayer play];
            [self.mainMenuViewController levelsSelected];
            [self.ship removeAllActions];
        }
        else if ([[self nodeAtPoint:location] isEqualToNode:self.settingsButton])
        {
            [self.clickSoundPlayer play];
            [self.mainMenuViewController settingsSelected];
            [self.ship removeAllActions];
        }
    }
}

//-(void)setBackgroundMusic:(NSString*)filename
//{
//    NSError *error;
//    NSURL *backgroundMusicURL = [[NSBundle mainBundle] URLForResource:filename withExtension:nil];
//    
//    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
//    
//    if (!appDelegate.backgroundMusicPlayer)
//    {
//        appDelegate.backgroundMusicPlayer = nil;
//        appDelegate.backgroundMusicPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:backgroundMusicURL error:&error];
//        appDelegate.backgroundMusicPlayer.numberOfLoops = -1;
//        appDelegate.backgroundMusicPlayer.volume = 1.0;
//        [appDelegate.backgroundMusicPlayer prepareToPlay];
//    }
//    
//    if (!appDelegate.backgroundMusicPlayer.isPlaying)
//    {
//        [appDelegate.backgroundMusicPlayer play];
//    }
//}

-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
}

-(void)setupShipFlyingFrames
{
    NSMutableArray* frames = [[NSMutableArray alloc] init];
    NSInteger frameCount = 119;
    
    for (int i = 0; i < frameCount; i++)
    {
        NSString* imageName = [[NSString alloc] init];
        
        if (i < 10)
        {
            imageName = [NSString stringWithFormat:@"Comp 2_0000%d", i];
        }
        else if (i<100)
        {
            imageName = [NSString stringWithFormat:@"Comp 2_000%d", i];
        }
        else
        {
            imageName = [NSString stringWithFormat:@"Comp 2_00%d", i];
        }
        SKTexture *tmpTexture = [SKTexture textureWithImage:[UIImage imageNamed:imageName]];
        [frames addObject:tmpTexture];
        
    }
    self.shipFlyingFrames = [[NSArray alloc] initWithArray:frames];
    
}

- (void)flyShip
{
    [self.ship runAction:[SKAction repeatActionForever:
                      [SKAction animateWithTextures:self.shipFlyingFrames
                                       timePerFrame:0.025f
                                             resize:NO
                                            restore:YES]] withKey:@"FlyingShip"];
    return;
}


@end
