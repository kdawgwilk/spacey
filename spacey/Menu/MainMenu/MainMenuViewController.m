//
//  MainMenuViewController.m
//  code_camp_2015
//
//  Created by Chad Olsen on 11/13/15.
//  Copyright © 2015 Kraig. All rights reserved.
//

#import "MainMenuViewController.h"
#import "MainMenu.h"
#import "GameViewController.h"
#import "Player.h"
@import AVFoundation;
#import "SPCYButton.h"
#import "SPCYSettings.h"

@interface MainMenuViewController()

@property (nonatomic) AVAudioPlayer * backgroundMusicPlayer;
@property (nonatomic, strong) SKView* skView;

@end

@implementation SKScene (Unarchive)

+ (instancetype)unarchiveFromFile:(NSString *)file {
    /* Retrieve scene file path from the application bundle */
    NSString *nodePath = [[NSBundle mainBundle] pathForResource:file ofType:@"sks"];
    /* Unarchive the file to an SKScene object */
    NSData *data = [NSData dataWithContentsOfFile:nodePath
                                          options:NSDataReadingMappedIfSafe
                                            error:nil];
    NSKeyedUnarchiver *arch = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
    [arch setClass:self forClassName:@"SKScene"];
    SKScene *scene = [arch decodeObjectForKey:NSKeyedArchiveRootObjectKey];
    [arch finishDecoding];
    
    return scene;
}

@end

@implementation MainMenuViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Configure the view.
    self.skView = (SKView *)self.view;
    /* Sprite Kit applies additional optimizations to improve rendering performance */
    self.skView.ignoresSiblingOrder = YES;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // Create and configure the scene.
    self.mainMenu = [MainMenu unarchiveFromFile:@"MainMenuScene"];
    [self.mainMenu setSize:CGSizeMake(self.view.frame.size.width, self.view.frame.size.height)];
    self.mainMenu.scaleMode = SKSceneScaleModeResizeFill;
    self.mainMenu.mainMenuViewController = self;
    
    // Present the scene.
    [self.skView presentScene:self.mainMenu];
    
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    [self.mainMenu flyShip];
    
    [self setupButtons];
    
    [self startSound];
}

- (void)setupButtons
{
    float horizontalPaddingBetweenButtons = 5;
    float margin = 10;
    float buttonHeight = 50;
    float buttonY = self.view.frame.size.height - buttonHeight - margin;
    float middleButtonWidth = self.view.frame.size.width * 0.45;
    float sideButtonWidth = (self.view.frame.size.width - middleButtonWidth - (2 * margin) - (2 * horizontalPaddingBetweenButtons)) / 2;
    
    float x = margin;
    SPCYButton* settingsButton = [SPCYButton buttonWithFrame:CGRectMake(x, buttonY, sideButtonWidth, buttonHeight) overlayImage:[UIImage imageNamed:@"settings_overlay"] andBackgroundColor:[SPCYSettings settingsButtonColor]];
    [settingsButton addTarget:self action:@selector(settingsSelected) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:settingsButton];
    
    x += horizontalPaddingBetweenButtons + sideButtonWidth;
    
    SPCYButton* playButton = [SPCYButton buttonWithFrame:CGRectMake(x, buttonY, middleButtonWidth, buttonHeight) overlayImage:[UIImage imageNamed:@"play_overlay"] andBackgroundColor:[SPCYSettings playButtonColor]];
    [playButton addTarget:self action:@selector(playSelected) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:playButton];
    
    x += horizontalPaddingBetweenButtons + middleButtonWidth;
    
    SPCYButton* levelsButton = [SPCYButton buttonWithFrame:CGRectMake(x, buttonY, sideButtonWidth, buttonHeight) overlayImage:[UIImage imageNamed:@"menu_overlay"] andBackgroundColor:[SPCYSettings levelsButtonColor]];
    [levelsButton addTarget:self action:@selector(levelsSelected) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:levelsButton];
}

- (void)startSound
{
    if (![self.backgroundMusicPlayer isPlaying])
    {
        NSError *error;
        NSURL * backgroundMusicURL = [[NSBundle mainBundle] URLForResource:@"background_music" withExtension:@"mp3"];
        self.backgroundMusicPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:backgroundMusicURL error:&error];
        self.backgroundMusicPlayer.numberOfLoops = -1;
        [self.backgroundMusicPlayer prepareToPlay];
        [self.backgroundMusicPlayer setVolume:0.35];
        [self.backgroundMusicPlayer play];
    }
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    } else {
        return UIInterfaceOrientationMaskAll;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - Actions

- (void)playSelected
{
    [self performSegueWithIdentifier:@"Menu To Game" sender:nil];
}

- (void)levelsSelected
{
    [self performSegueWithIdentifier:@"Menu To Levels" sender:nil];
}
- (void)settingsSelected
{
    [self performSegueWithIdentifier:@"Menu To Settings" sender:nil];
}

- (void)aboutSelected
{
    
}

- (IBAction)unwindToMainMenu:(UIStoryboardSegue *)unwindSegue
{
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"Menu To Game"])
    {
        GameViewController* vc = [segue destinationViewController];
        vc.levelNumber = [Player getPlayersCurrentLevelFromPlist];
        [self.backgroundMusicPlayer stop];
    }
    
    
    [self performSelector:@selector(endScene) withObject:nil afterDelay:1.0];
}

- (void)endScene // this is supposed to clean up the skview for performance
{
    [self.mainMenu removeAllActions];
    [self.mainMenu removeAllChildren];
    [self.mainMenu removeFromParent];
    [self.skView presentScene:nil];
    [self.skView removeFromSuperview];
}

@end
