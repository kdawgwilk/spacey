//
//  MainMenu.h
//  code_camp_2015
//
//  Created by Chad Olsen on 11/13/15.
//  Copyright © 2015 Kraig. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "MainMenuViewController.h"

@interface MainMenu : SKScene

@property (nonatomic, weak) MainMenuViewController* mainMenuViewController;

- (void)flyShip;

@end
