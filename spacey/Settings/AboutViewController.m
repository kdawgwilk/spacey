//
//  AboutViewController.m
//  code_camp_2015
//
//  Created by Chad Olsen on 11/14/15.
//  Copyright © 2015 Kraig. All rights reserved.
//

#import "AboutViewController.h"

@interface AboutViewController ()

@property (nonatomic, strong) NSArray* dataSource;
@property (nonatomic) NSInteger cellLabelWidth;

@end

@implementation AboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // set nav bar title to be white
    UIBarButtonItem* backButton = [[UIBarButtonItem alloc] initWithTitle:@"< Back" style:UIBarButtonItemStyleDone target:self action:@selector(backButtonPressed:)];
    [backButton setTintColor:[UIColor whiteColor]];
    [self.navigationItem setLeftBarButtonItem:backButton];
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    [self.tableView setBackgroundView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"background"]]];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    self.cellLabelWidth = self.tableView.frame.size.width - 32;
    
    [self setupDataSource];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.dataSource count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self heightForCellAtRow:indexPath.row];
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.userInteractionEnabled = NO;
        cell.textLabel.textColor = [UIColor whiteColor];
    }
    
    [self configureCell:cell withType:[[self.dataSource objectAtIndex:indexPath.row] objectForKey:@"type"] andValue:[[self.dataSource objectAtIndex:indexPath.row] objectForKey:@"value"]];
    
    cell.textLabel.text = [[self.dataSource objectAtIndex:indexPath.row] objectForKey:@"value"];
    
    return cell;
}

- (void)configureCell:(UITableViewCell*)cell withType:(NSString*)type andValue:(NSString*)value
{
    [cell.textLabel setText:value];
    [cell.textLabel setFrame:CGRectMake(0, 0, self.cellLabelWidth, 0)];
    [cell.textLabel sizeToFit];
    
    if ([type isEqualToString:@"t1"])
    {
        [cell.textLabel setTextAlignment:NSTextAlignmentCenter];
        [cell.textLabel setFont:[UIFont systemFontOfSize:20 weight:UIFontWeightRegular]];
        [cell.textLabel setNumberOfLines:1];
        [cell.textLabel setLineBreakMode:NSLineBreakByClipping];
    }
    else if ([type isEqualToString:@"p1"])
    {
        [cell.textLabel setTextAlignment:NSTextAlignmentLeft];
        [cell.textLabel setFont:[UIFont systemFontOfSize:16 weight:UIFontWeightUltraLight]];
        [cell.textLabel setNumberOfLines:0];
        [cell.textLabel setLineBreakMode:NSLineBreakByWordWrapping];
    }
}

- (NSInteger)heightForCellAtRow:(NSInteger)row
{
    NSString* text = [[self.dataSource objectAtIndex:row] objectForKey:@"value"];
    NSString* type = [[self.dataSource objectAtIndex:row] objectForKey:@"type"];
    UIFont* font;
    if ([type isEqualToString:@"t1"])
    {
        font = [UIFont systemFontOfSize:20 weight:UIFontWeightRegular];
    }
    else if ([type isEqualToString:@"p1"])
    {
        font = [UIFont systemFontOfSize:16 weight:UIFontWeightUltraLight];
    }
    
    NSInteger height = 0;
    height += 5; // padding
    
    UILabel* labelForHeightCalculation = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.cellLabelWidth, 0)];
    [labelForHeightCalculation setText:text];
    
    [labelForHeightCalculation setLineBreakMode:NSLineBreakByWordWrapping];
    [labelForHeightCalculation setNumberOfLines:0];
    [labelForHeightCalculation sizeToFit];
    height += labelForHeightCalculation.frame.size.height;
    
    height += 5; // padding
    
    return height;
}

- (void)setupDataSource
{
    self.dataSource = @[
                        @{@"type" : @"t1",         @"value" : @"This is the title"},
                        @{@"type" : @"p1",     @"value" : @"Here's a bunch of works that might fit the the the the the teh !"},
                        @{@"type" : @"p1",     @"value" : @"Here's the third line"},
                        ];
}

#pragma mark - actions

- (void)backButtonPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
